<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>
<meta name="XAR Files" content="index_htm_files/xr_files.txt"/>
 <title>Add Driver</title>
 <meta http-equiv="Content-Type" content="text/html; charset=Windows-874"/>
 <meta name="Generator" content="Xara HTML filter v.4.0.4.653"/>
 <link rel="stylesheet" type="text/css" href="index_htm_files/xr_main.css"/>
 <link rel="stylesheet" type="text/css" href="index_htm_files/xr_text.css"/>
 <script type="text/javascript" src="index_htm_files/roe.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<title>Form</title>
<link href="css/FormCss_1.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="css/FormCss_2.css" />
<style type="text/css">
    .form-label{
        width:150px !important;
    }
    .form-label-left{
        width:150px !important;
    }
    .form-line{
        padding-top:12px;
        padding-bottom:12px;
    }
    .form-label-right{
        width:150px !important;
    }
    body, html{
        margin:0;
        padding:0;
        background:false;
    }

    .form-all{
        margin:0px auto;
        padding-top:20px;
        width:650px;
        color:#555 !important;
        font-family:'Lucida Grande';
        font-size:14px;
    }
    .form-radio-item label, .form-checkbox-item label, .form-grading-label, .form-header{
        color:#555;
    }

</style>

<script src="js/FormJS_1.JS" type="text/javascript"></script>
<script type="text/javascript">
   JotForm.init();
</script>
<link href="css/myCustom.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--[if IE]><div class="xr_ap" id="xr_xri" style="width: 1360px; height: 800px; clip: rect(0px 1360px 800px 0px);"><![endif]-->
<!--[if !IE]>--><div class="xr_ap" id="xr_xri" style="width: 1360px; height: 800px; clip: rect(0px, 1360px, 800px, 0px);"><!--<![endif]-->
 <img class="xr_ap" src="index_htm_files/9.png" alt="" title="" style="left: 1px; top: 4px; width: 1360px; height: 796px;"/>
 
 <label class="nor_text" id="he1" >
          Add Driver
        </label>
        <br />
<form class="form_pos2" action="./adddriver2" method="post" name="form_4" id="4" >
  
  <div class="form-all">
    <ul class="form-section">
      <li class="form-line" id="id_first_name">
        <label class="form-label-left" id="label_first_name" for="input_first_name">
          First Name<span class="form-required">*</span>
        </label>
        <div id="cid_first_name" class="form-input">
          <input type="text" class="form-textbox validate[required]" id="input_first_name" name="first_name" size="20" />
        </div>
      </li>
      <li class="form-line" id="id_last_name">
        <label class="form-label-left" id="label_last_name" for="input_last_name">
          Last Name<span class="form-required">*</span>
        </label>
        <div id="cid_1" class="form-input">
          <input type="text" class="form-textbox validate[required]" id="input_last_name" name="last_name" size="20" />
        </div>
      </li>
      <li class="form-line" id="id_birthday">
       <label class="form-label-left" id="label_birthday">
          Birthday
        </label><br /><br />
                
        <label class="form-label-left1" id="label_birthday_1" for="input_birthday_1">
          Year<span class="form-required">*</span>
        </label>        
          <select class="form-dropdown validate[required]" style="width:70px" id="input_birthday_1" name="year">
            <%
            out.println("<option value=\"\"> </option>");
            for(int i=1900;i<=2500;i++){				
					out.println("<option value=\""+i+"\">"+i+"</option>");					
				
			} %>
          </select>
          <label class="form-label-left1" id="label_birthday_2" for="input_birthday_2">
          Month<span class="form-required">*</span>
        </label>        
          <select class="form-dropdown validate[required]" style="width:50px" id="input_birthday_2" name="month">
            <%
            out.println("<option value=\"\"> </option>");
            for(int i=1;i<=12;i++){		
            	out.println("<option value=\""+i+"\">"+i+"</option>");					
				
			} %>
          </select>
          <label class="form-label-left1" id="label_birthday_3" for="input_birthday_3">
          Day<span class="form-required">*</span>
        </label>        
          <select class="form-dropdown validate[required]" style="width:50px" id="input_birthday_3" name="day">
            <%out.println("<option value=\"\"> </option>");
            for(int i=1;i<=31;i++){
				
            	out.println("<option value=\""+i+"\">"+i+"</option>");
					
				
			} %>
          </select>
        
      </li>
      <li class="form-line" id="id_sex">
        <label class="form-label-left" id="label_sex" for="input_sex">
          Sex<span class="form-required">*</span>
        </label>
        <select class="form-dropdown validate[required]" style="width:50px" id="input_sex" name="sex">
            <%out.println("<option value=\"\"> </option>");		
            	out.println("<option value=\"Male\">Male</option>");
            	out.println("<option value=\"Female\">Female</option>");
			 %>
          </select>
      </li>
      <li class="form-line" id="id_4">
        <label class="form-label-left" id="label_4" for="input_4">
          Country<span class="form-required">*</span>
        </label>
        <div id="cid_4" class="form-input">
          <input type="text" class="form-textbox validate[required]" id="input_4" name="country" size="20" />
        </div>
      </li>
      <li class="form-line" id="id_3">
        <label class="form-label-left" id="label_3" for="input_3">
          Province<span class="form-required">*</span>
        </label>
        <div id="cid_3" class="form-input">
          <input type="text" class="form-textbox validate[required]" id="input_3" name="province" size="20" />
        </div>
      </li>
      <li class="form-line" id="id_5">
        <label class="form-label-left" id="label_5" for="input_5">
          City<span class="form-required">*</span>
        </label>
        <div id="cid_5" class="form-input">
          <input type="text" class="form-textbox validate[required]" id="input_5" name="city" size="20" />
        </div>
      </li>
      <li class="form-line" id="id_7">
        <label class="form-label-left" id="label_7" for="input_7">
          Address<span class="form-required">*</span>
        </label>
        <div id="cid_7" class="form-input">
          <textarea rows="5" cols="40"  class="form-textbox validate[required]" id="input_7" name="address"  ></textarea>
        </div>
      </li>
      <li class="form-line" id="id_email">
        <label class="form-label-left" id="label_email" for="input_email">
          E-Mail<span class="form-required">*</span>
        </label>
        <div id="cid_email" class="form-input">
          <input type="text" class="form-textbox validate[required]" id="input_email" name="email" size="20" />
        </div>
      </li>
      <li class="form-line" id="id_6">
        <label class="form-label-left" id="label_6" for="input_6">
          Phone<span class="form-required">*</span>
        </label>
        <div id="cid_6" class="form-input">
          <input type="text" class="form-textbox validate[required]" id="input_6" name="phone" size="20" />
        </div>
      </li>
      <li class="form-line" id="id_2">
        <div id="cid_2" class="form-input-wide">
          <div style="margin-left:156px" class="form-buttons-wrapper">
            <button id="input_2" type="submit" class="form-submit-button">
              Add
            </button>
          </div>
        </div>
      </li>
      <li style="display:none">
        Should be Empty:
        <input type="text" name="website" value="" />
      </li>
    </ul>
  </div>
  
</form></div></body>

</html>
