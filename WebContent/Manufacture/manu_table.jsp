<%@page import="getdat.getDatabase"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="java.util.*,com.mongodb.*"%>
<%
	HttpSession ses = request.getSession();
	String usn = (String) ses.getAttribute("usn");
	String pwd = (String) ses.getAttribute("pwd");
	String user_stat = (String) ses.getAttribute("user_stat");
	String company_name=(String) ses.getAttribute("company_name");
	String company_id=(String) ses.getAttribute("company_id");
	String manu_id1 = request.getParameter("manu_id");
	String manu_id =new String(manu_id1.getBytes("ISO-8859-1"), "UTF8");
	if(user_stat.equals("")||user_stat==null){
		
		String wr="Wrong ID or Password";
		request.setAttribute("wrong", wr);
		RequestDispatcher view = request.getRequestDispatcher("start.jsp?note="+wr+"");
		view.forward(request, response);
					    
		}	
	String root_url=(String)ses.getAttribute("root_url");
	BasicDBObject tquery =new BasicDBObject();
	tquery.put("Company_ID", company_id);
	tquery.put("Manufacture_ID", manu_id);
	int row1 = getDatabase.getRowCount("manufacture_table", tquery);
	DBObject[] table_manu =getDatabase.getCollData("manufacture_table", tquery, row1);
	String[] inhand = new String[row1+1];
	String[] amount = new String[row1+1];
	String[] fore = new String[row1+1];
	String[] left = new String[row1+1];
	for(int i=1;i<=row1;i++){
		inhand[i]=(String)table_manu[i].get("inhand");
		amount[i]=(String)table_manu[i].get("amount");
		fore[i]=(String)table_manu[i].get("sell_forecast");
		left[i]=(String)table_manu[i].get("left");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Manufacture Table</title>
<script type="text/javascript" charset="UTF-8" src="./js/manu_table_util.js"></script>
</head>
<body>
<h3 style="position: absolute; left:40%; top:10px;">ตารางการผลิตหลัก ของสินค้ารหัส : <% out.print(manu_id); %></h3>
<input type="button" value="เพิ่มแผนการผลิตสินค้า" onclick="addRow()" style="position: absolute; left:100px; top:60px;"/>
<input type="button" value="ลบแผนของสับดาห์ล่าสุด" onclick="deletelastRow()" style="position: absolute; left:250px; top:60px;"/>
<input type="button" value="เสร็จสิ้น" onclick="submit_form()" style="position: absolute; left:500px; top:60px;"/>
<form id="form_1" name="form_1" method="post" action="add_manu_table.ts">
<input id="num_pm" name="num_pm" type="hidden" value="<% out.print(row1); %>"/>
<input id="manu_id" name="manu_id" type="hidden" value="<% out.print(manu_id); %>"/>
<div style="position: absolute; left:50px; top:100px;width:50%; height:500px ;overflow:scroll;">
<table id="table_dat1" border="1" style="position: absolute;left:0%; top:0%;width:100%;">
  <tr>
    <th>สัปดาห์การผลิต</th>
    <th>สินค้าในมือ</th>
    <th>จำนวนการผลิต</th>
    <th>พยากรณ์การขาย</th>
    <th>คงเหลือ</th>    
  </tr> 
  <% 
  for(int i=1;i<=row1;i++){
	  out.println("<tr>");
	  out.println("<td>Week "+i+"<input id=\"week_"+i+"\" name=\"week_"+i+"\" type=\"hidden\" value=\""+i+"\" style=\"width:70px\"/></td>");
	  out.println("<td><input id=\"inhand_week_"+i+"\" name=\"inhand_week_"+i+"\" type=\"text\" value=\""+inhand[i]+"\"style=\"width:70px\"/></td>");
	  out.println("<td><div id=\"amount_d_week_"+i+"\"><input id=\"amount_week_"+i+"\" name=\"amount_week_"+i+"\" type=\"text\" value=\""+amount[i]+"\"style=\"width:70px\"/></div></td>");
	  out.println("<td><div id=\"fore_d_week_"+i+"\"><input id=\"fore_week_"+i+"\" name=\"fore_week_"+i+"\" type=\"text\" value=\""+fore[i]+"\"style=\"width:70px\"/></div></td>");
	  out.println("<td><div id=\"left_d_week_"+i+"\"><input id=\"left_week_"+i+"\" name=\"left_week_"+i+"\" type=\"text\" value=\""+left[i]+"\"style=\"width:70px\"/></div></td>");
	  out.println("</tr>");
  }
 
  %> 
  </table>  
</div>
</form>
<div style="position: absolute;left:60%; top:100px;width:40%;">
<label style="position: absolute;left:200px; top:0px;">คำนวนการผลิต เติมข้อมูลให้ครบ</label>
<label id ="p_left1" style="position: absolute;left:0px; top:40px;">สินค้าคงเหลือเริ่มต้น</label>
<input id="p_left1_a" name="p_left1_a" type="text" value="0" style="position: absolute;left:300px; top:40px;"/>
<label id="p_amount" style="position: absolute;left:0px; top:80px;">จำนวนการผลิต</label>
<label id="p_amount_a" style="position: absolute;left:300px; top:80px;">0</label>
<label id="p_sell" style="position: absolute;left:0px; top:120px;">พยากรณ์ขายปัจจุบัน</label>
<input id="p_sell_a" name="p_sell_a" type="text" value="0" style="position: absolute;left:300px; top:120px;"/>
<label id="p_sell2" style="position: absolute;left:0px; top:160px;">พยากรณ์ขายสับดาห์ถัดไป</label>
<input id="p_sell2_a" name="p_sell2_a" type="text" value="0" style="position: absolute;left:300px; top:160px;"/>
<label id="p_gu" style="position: absolute;left:0px; top:200px;">จำนวนสินค้าป้องกันการขาดมือ</label>
<input id="p_gu_a" name="p_gu_a" type="text" value="0" style="position: absolute;left:300px; top:200px;"/>
<label id="p_left2" style="position: absolute;left:0px; top:240px;">คงเหลือ</label>
<label id="p_left2_a" style="position: absolute;left:300px; top:240px;">0</label>
<input type="button" value="คำนวน" onclick="cal_plan()" style="position: absolute; left:160px; top:300px;"/>
</div>
</body>
</html>