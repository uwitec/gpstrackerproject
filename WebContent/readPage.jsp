<%@ page import="java.util.*,com.mongodb.*" %>
<%
HttpSession ses = request.getSession();
ses.setAttribute("wr1", "Session out");
	DBObject dat =(DBObject)ses.getAttribute("dbdat");
	String tsk=(String)ses.getAttribute("task");	
	String usn =(String) ses.getAttribute("usn");
	String pwd =(String) ses.getAttribute("pwd");
	String user_stat=getdat.getDatabase.getUserdata(usn, pwd);
	String company_id = (String) ses.getAttribute("company_id");
	if(user_stat.equals("")){
		
		String wr="Session out";
		request.setAttribute("wrong", wr);
		RequestDispatcher view = request.getRequestDispatcher("start.jsp?p1=123456");
		view.forward(request, response);
					    
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <meta name="XAR Files" content="index_htm_files/xr_files.txt"/>
 <title>Read & Edit Page</title>
 <meta http-equiv="Content-Type" content="text/html; charset=Windows-874"/>
 <meta name="Generator" content="Xara HTML filter v.4.0.4.653"/>
 <link rel="stylesheet" type="text/css" href="index_htm_files/xr_main.css"/>
 <link rel="stylesheet" type="text/css" href="index_htm_files/xr_text.css"/>
 <link href="css/myCustom.css" rel="stylesheet" type="text/css" />
 <script type="text/javascript" src="index_htm_files/roe.js"></script>
 <link href="css/FormCss_1.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="css/FormCss_2.css" />
<style type="text/css">
    .form-label{
        width:150px !important;
    }
    .form-label-left{
        width:150px !important;
    }
    .form-line{
        padding-top:12px;
        padding-bottom:12px;
    }
    .form-label-right{
        width:150px !important;
    }
    body, html{
        margin:0;
        padding:0;
        background:false;
    }

    .form-all{
        margin:0px auto;
        padding-top:20px;
        width:650px;
        color:#555 !important;
        font-family:'Lucida Grande';
        font-size:14px;
    }
    .form-radio-item label, .form-checkbox-item label, .form-grading-label, .form-header{
        color:#555;
    }

</style>


<link href="css/myCustom.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="xr_ap" id="xr_xr" style="width: 1360px; height: 800px; top:0px; left:0px; margin-left: -680px;">
 <script type="text/javascript">var xr_xr=document.getElementById("xr_xr")</script>
<!--[if IE]><div class="xr_ap" id="xr_xri" style="width: 1360px; height: 800px; clip: rect(0px 1360px 800px 0px);"><![endif]-->
<!--[if !IE]>--><div class="xr_ap" id="xr_xri" style="width: 1360px; height: 800px; clip: rect(0px, 1360px, 800px, 0px);"><!--<![endif]-->
 <img class="xr_ap" src="index_htm_files/9.png" alt="" title="" style="left: 1px; top: 4px; width: 1360px; height: 796px;"/>
 <div id="xr_xo0" class="xr_ap" style="left:0; top:0; width:1360px; height:100px; visibility:hidden;">
 <a href="" onclick="return(false);">
 </a>
 </div> 
 <h1 class="form_deli_2" >Edit Detail</h1>
 <div id="m_other" style="position: absolute; left:50px; top:50px;width: 580px; height: 300px;">
 <iframe id="iframe_e2" style="height:100%;width:100%;"></iframe>
 </div> 
 <div id="m_detail" style="position: absolute; left:650px; top:50px;width: 600px; height: 700px;">
 <iframe id="iframe_e" style="height:100%;width:100%;"></iframe>
 </div> 
 <div class="read_page">

<% 
	if(tsk.equalsIgnoreCase("deliver")){		
		int row = 0;
		row = getdat.getDatabase.getRowCount("deliver", "Company_ID", company_id);
		DBObject[] deviceDat =null;
		deviceDat=getdat.getDatabase.getCollData("deliver", "Company_ID", company_id, row);
		String[] deliver_id =new String[row+1];
		String[] vehicle_id =new String[row+1];
		String[] driver_id=new String[row+1];
		String[] destination=new String[row+1];
		String[] origin=new String[row+1];
		String[] stat=new String[row+1];
		for(int i=1;i<=row;i++){
			driver_id[i]=(String) deviceDat[i].get("Driver_ID");
			deliver_id[i]=(String) deviceDat[i].get("Deliver_ID");
			vehicle_id[i]=(String) deviceDat[i].get("Vehicle_ID");
			stat[i]=(String) deviceDat[i].get("stat");
			origin[i]=(String) deviceDat[i].get("Origin");
			destination[i]=(String) deviceDat[i].get("Destination");
			
			
		}
		out.println("<table class=\"read_form\" border=\"1\">");
		out.println("<caption>Deliver Detail</caption>");
		out.println("<tr><th>Deliver ID</th><th>Driver ID</th><th>Vehicle ID</th></tr>");
		for(int i=1;i<=row;i++){
			out.println("<tr><td>"+deliver_id[i]+"</td><td><a href=\"./E_edit?dv="+driver_id[i]+"&dd=0&task=driver\" target=\"iframe_e\">"+driver_id[i]+"</a></td><td><a href=\"./E_edit?dv="+vehicle_id[i]+"&dd=0&task=vehicle\" target=\"iframe_e\">"+vehicle_id[i]+"</a></td>");
			out.println("<td>");
			out.println("<form name=\"formmy"+i+"\"action=\"E_deliver\" method=\"post\">");
			//out.println("<input type=\"checkbox\" id=\"dv\" checked=\"checked\" name=\"dv\" value=\"testttttt\" /> ");
			out.println("<input type=\"button\" value=\"Product Detail\" onclick=\"window.open('./viewProduct.jsp?dv="+deliver_id[i]+"','iframe_e2')\"/>");
			out.println("<input type=\"button\" value=\"Edit\" onclick=\"window.open('./E_edit?dv="+deliver_id[i]+"&dd=0&task=deliver','iframe_e')\"/>");
			out.println("<input type=\"button\" value=\"Delete\" onclick=\"window.location.href='./E_edit?dv="+deliver_id[i]+"&dd=yes&task=deliver'\"/>");
			out.println("</form>");
			out.println("</td>");
			out.println("</tr>");
		}
		out.println("</table>");
		
	}else if(tsk.equalsIgnoreCase("device")){
		int row = 0;
		row = getdat.getDatabase.getRowCount("device", "Company_ID", company_id);
		DBObject[] deviceDat =null;
		deviceDat=getdat.getDatabase.getDeviceCompany(company_id, row);
		String[] device_id =new String[row+1];
		String[] vehicle_id =new String[row+1];
		String[] imei=new String[row+1];
		String[] stat=new String[row+1];
		for(int i=1;i<=row;i++){
			device_id[i]=(String) deviceDat[i].get("Device_ID");
			vehicle_id[i]=(String) deviceDat[i].get("Vehicle_ID");
			if(vehicle_id[i].equalsIgnoreCase("")){
				vehicle_id[i]="don't add to any vehicle";
			}else{
				String tmp=vehicle_id[i];
				vehicle_id[i]="<a href=\"./E_edit?dv="+tmp+"&dd=0&task=vehicle\" target=\"iframe_e\">"+tmp+"</a>";
			}
			imei[i]=(String) deviceDat[i].get("imei");
			stat[i]=(String) deviceDat[i].get("stat");
		}
		out.println("<table class=\"read_form\" border=\"1\">");
		out.println("<caption>Device Detail</caption>");
		out.println("<tr><th>Device ID</th><th>Vehicle ID</th><th>IMEI ID</th></tr>");
		for(int i=1;i<=row;i++){
			out.println("<tr><td>"+device_id[i]+"</td><td>"+vehicle_id[i]+"</td><td>"+imei[i]+ "</td>");
			out.println("<td>");
			out.println("<form name=\"formmy"+i+"\"action=\"E_device\" method=\"post\">");
			//out.println("<input type=\"checkbox\" id=\"dv\" checked=\"checked\" name=\"dv\" value=\"testttttt\" /> ");
			out.println("<input type=\"button\" value=\"View & Edit\" onclick=\"window.open('./E_edit?dv="+device_id[i]+"&dd=0&task=device','iframe_e')\"/>");
			out.println("<input type=\"button\" value=\"Delete\" onclick=\"window.location.href='./E_edit?dv="+device_id[i]+"&dd=yes&task=device'\"/>");
			out.println("</form>");
			out.println("</td>");
			out.println("</tr>");
		}
		out.println("</table>");
	}else if(tsk.equalsIgnoreCase("driver")){
		int row = 0;
		row = getdat.getDatabase.getRowCount("driver", "Company_ID", company_id);
		DBObject[] deviceDat =null;
		deviceDat=getdat.getDatabase.getCollData("driver", "Company_ID", company_id, row);
		String[] driver_id =new String[row+1];
		String[] first_name =new String[row+1];
		String[] last_name=new String[row+1];
		String[] province=new String[row+1];
		String[] city=new String[row+1];
		String[] address=new String[row+1];
		String[] phone=new String[row+1];
		String[] stat=new String[row+1];
		String[] year=new String[row+1];
		String[] month=new String[row+1];
		String[] day=new String[row+1];
		String[] sex=new String[row+1];
		String[] email=new String[row+1];
		for(int i=1;i<=row;i++){
			driver_id[i]=(String) deviceDat[i].get("Driver_ID");
			first_name[i]=(String) deviceDat[i].get("First_name");
			last_name[i]=(String) deviceDat[i].get("Last_name");
			stat[i]=(String) deviceDat[i].get("stat");
			year[i]=(String) deviceDat[i].get("Year");
			month[i]=(String) deviceDat[i].get("Month");
			day[i]=(String) deviceDat[i].get("Day");
			sex[i]=(String) deviceDat[i].get("Sex");
			email[i]=(String) deviceDat[i].get("E-Mail");
			province[i]=(String) deviceDat[i].get("Province");
			city[i]=(String) deviceDat[i].get("City");
			address[i]=(String) deviceDat[i].get("Address");
			phone[i]=(String) deviceDat[i].get("Phone");
			
		}
		out.println("<table class=\"read_form\" border=\"1\">");
		out.println("<caption>Driver Detail</caption>");
		out.println("<tr><th>Driver ID</th><th>Name</th><th>Birth Day</th><th>Sex</th></tr>");
		for(int i=1;i<=row;i++){
			out.println("<tr><td>"+driver_id[i]+"</td><td>"+first_name[i]+"  "+last_name[i]+"</td><td>"+year[i]+"/"+month[i]+"/"+day[i]+"</td><td>"+sex[i]+ "</td>");
			out.println("<td>");
			out.println("<form name=\"formmy"+i+"\"action=\"E_driver\" method=\"post\">");
			//out.println("<input type=\"checkbox\" id=\"dv\" checked=\"checked\" name=\"dv\" value=\"testttttt\" /> ");
			out.println("<input type=\"button\" value=\"View & Edit\" onclick=\"window.open('./E_edit?dv="+driver_id[i]+"&dd=0&task=driver','iframe_e')\"/>");
			out.println("<input type=\"button\" value=\"Delete\" onclick=\"window.location.href='./E_edit?dv="+driver_id[i]+"&dd=yes&task=driver'\"/>");
			out.println("</form>");
			out.println("</td>");
			out.println("</tr>");
		}
		out.println("</table>");
	}else if(tsk.equalsIgnoreCase("vehicle")){
		int row = 0;
		row = getdat.getDatabase.getRowCount("vehicle", "Company_ID", company_id);
		DBObject[] deviceDat =null;
		deviceDat=getdat.getDatabase.getCollData("vehicle", "Company_ID", company_id, row);
		String[] device_id =new String[row+1];
		String[] vehicle_id =new String[row+1];
		String[] registerID=new String[row+1];
		String[] year=new String[row+1];
		String[] month=new String[row+1];
		String[] day=new String[row+1];
		String[] gear=new String[row+1];
		String[] brand=new String[row+1];
		String[] model=new String[row+1];
		String[] color=new String[row+1];
		String[] description=new String[row+1];
		String[] engine=new String[row+1];
		String[] stat=new String[row+1];
		String[] type=new String[row+1];
		for(int i=1;i<=row;i++){
			device_id[i]=(String) deviceDat[i].get("Device_ID");
			vehicle_id[i]=(String) deviceDat[i].get("Vehicle_ID");
			registerID[i]=(String) deviceDat[i].get("RegisterID");
			stat[i]=(String) deviceDat[i].get("stat");
			year[i]=(String) deviceDat[i].get("Year");
			month[i]=(String) deviceDat[i].get("Month");
			day[i]=(String) deviceDat[i].get("Day");
			gear[i]=(String) deviceDat[i].get("Gear");
			brand[i]=(String) deviceDat[i].get("Brand");
			model[i]=(String) deviceDat[i].get("Model");
			color[i]=(String) deviceDat[i].get("Color");
			description[i]=(String) deviceDat[i].get("Description");
			engine[i]=(String) deviceDat[i].get("Engine");
			stat[i]=(String) deviceDat[i].get("stat");
			type[i]=(String) deviceDat[i].get("Type");
		}
		out.println("<table class=\"read_form\" border=\"1\">");
		out.println("<caption>Vehicle Detail</caption>");
		out.println("<tr><th>Device ID</th><th>Vehicle ID</th></tr>");
		for(int i=1;i<=row;i++){
			out.println("<tr><td><a href=\"./E_edit?dv="+device_id[i]+"&dd=0&task=device\" target=\"iframe_e\">"+device_id[i]+"</a></td><td>"+vehicle_id[i]+"</td>");
			out.println("<td>");
			out.println("<form name=\"formmy"+i+"\"action=\"E_vehicle\" method=\"post\">");
			//out.println("<input type=\"checkbox\" id=\"dv\" checked=\"checked\" name=\"dv\" value=\"testttttt\" /> ");
			out.println("<input type=\"button\" value=\"View & Edit\" onclick=\"window.open('./E_edit?dv="+vehicle_id[i]+"&dd=0&task=vehicle','iframe_e')\" />");
			out.println("<input type=\"button\" value=\"Delete\" onclick=\"window.location.href='./E_edit?dv="+vehicle_id[i]+"&dd=yes&task=vehicle'\" />");
			out.println("</form>");
			out.println("</td>");
			out.println("</tr>");
		}
		out.println("</table>");
	}else if(tsk.equalsIgnoreCase("product")){
		ses.setAttribute("task2","product");
		ses.setAttribute("dat_id",dat.get("Product_ID"));
		out.print("<form name =\"edited\" action=\"updatesuccess.tas\" method=\"post\">"
				+"<input name=\"deliver_id\" type=\"hidden\" value =\""+dat.get("Deliver_ID")+"\">"
						+"<div>"
						+"<br />"
						+"<label class=\"form-label-left\" id=\"product_id1\" for=\"product_id\">"
						+"Product ID:"
						+"</label>"
				+"<input id=\"product_id\" name=\"product_id\" type=\"text\" value =\""+dat.get("Product_ID")+"\"><br />"
						+"</div>"				
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"product_name1\" for=\"product_name\">"
				+"Product Name:"
				+"</label>"
				+"<input id=\"product_name\" name=\"product_name\" type=\"text\" value =\""+dat.get("Product_Name")+"\"><br />"
						+"</div>"				
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"description1\" for=\"description\">"
				+"Description:"
				+"</label>"
				+"<textarea rows=\"5\" cols=\"40\" id=\"description\" name=\"description\" type=\"text\" value =\""+dat.get("Description")+"\">"+dat.get("Description")+"</textarea><br />"
						+"</div>"				
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"price1\" for=\"price\">"
				+"Price:"
				+"</label>"
				+"<input id=\"price\" name=\"price\" type=\"text\" value =\""+dat.get("Price")+"\"><br />"
						+"</div>"				
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"amount1\" for=\"amount\">"
				+"Amount:"
				+"</label>"
				+"<input id=\"amount\" name=\"amount\" type=\"text\" value =\""+dat.get("Amount")+"\"><br />"
						+"</div>"
				+"<input type=\"submit\" value =\"OK\"><br />"
				+"</form>");
	}else if(tsk.equalsIgnoreCase("customer")){
		int row = 0;
		row = getdat.getDatabase.getRowCount("customer", "Company_ID", company_id);
		DBObject[] deviceDat =null;
		deviceDat=getdat.getDatabase.getCollData("customer", "Company_ID", company_id, row);
		String[] customer_id =new String[row+1];
		String[] customer_name =new String[row+1];
		String[] year =new String[row+1];
		String[] month =new String[row+1];
		String[] day =new String[row+1];
		String[] sex =new String[row+1];
		String[] country=new String[row+1];
		String[] province=new String[row+1];
		String[] city=new String[row+1];
		String[] address=new String[row+1];
		String[] phone=new String[row+1];
		String[] email =new String[row+1];
		String[] stat=new String[row+1];
		for(int i=1;i<=row;i++){
			customer_id[i]=(String) deviceDat[i].get("Customer_ID");
			customer_name[i]=(String) deviceDat[i].get("Customer_name");
			country[i]=(String) deviceDat[i].get("Country");
			stat[i]=(String) deviceDat[i].get("stat");
			province[i]=(String) deviceDat[i].get("Province");
			city[i]=(String) deviceDat[i].get("City");
			address[i]=(String) deviceDat[i].get("Address");
			phone[i]=(String) deviceDat[i].get("Phone");
			year[i]=(String) deviceDat[i].get("Year");
			month[i]=(String) deviceDat[i].get("Month");
			day[i]=(String) deviceDat[i].get("Day");
			sex[i]=(String) deviceDat[i].get("Sex");
			email[i]=(String) deviceDat[i].get("E-Mail");
			
		}
		out.println("<table class=\"read_form\" border=\"1\">");
		out.println("<caption>Customer Detail</caption>");
		out.println("<tr><th>Customer ID</th><th>Customer Name</th><th>Date of Birth</th><th>Sex</th></tr>");
		for(int i=1;i<=row;i++){
			out.println("<tr><td>"+customer_id[i]+"</td><td>"+customer_name[i]+"</td><td>"+year[i]+"/"+month[i]+"/"+day[i]+"</td><td>"+sex[i]+"</td>");
			out.println("<td>");
			out.println("<form name=\"formmy"+i+"\"action=\"E_driver\" method=\"post\">");
			//out.println("<input type=\"checkbox\" id=\"dv\" checked=\"checked\" name=\"dv\" value=\"testttttt\" /> ");
			out.println("<input type=\"button\" value=\"View & Edit\" onclick=\"window.open('./E_edit?dv="+customer_id[i]+"&dd=0&task=customer','iframe_e')\"/><br />");
			out.println("<input type=\"button\" value=\"Delete\" onclick=\"window.location.href='./E_edit?dv="+customer_id[i]+"&dd=yes&task=customer'\"/>");
			out.println("</form>");
			out.println("</td>");
			out.println("</tr>");
		}
		out.println("</table>");
	}else{
		out.print("<br /> something Wrong");
	}
	
%>
 </div>
 <div id="xr_xd0"> 
 </div>
</div>
</div>
<!--[if lt IE 7]><script type="text/javascript" src="index_htm_files/png.js"></script><![endif]-->
<script type="text/javascript">xr_aeh()</script>
</body>
</html>