<%@ page import="java.util.*,com.mongodb.*" %>
<%
HttpSession ses = request.getSession();
ses.setAttribute("wr1", "Session out");
	DBObject dat =(DBObject)ses.getAttribute("dbdat");
	String tsk=(String)ses.getAttribute("task");
	String company_id=(String)ses.getAttribute("company_id");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <meta name="XAR Files" content="index_htm_files/xr_files.txt"/>
 <title>Edit Page</title>
 <meta http-equiv="Content-Type" content="text/html; charset=Windows-874"/>
 <meta name="Generator" content="Xara HTML filter v.4.0.4.653"/>
 <link rel="stylesheet" type="text/css" href="index_htm_files/xr_main.css"/>
 <link rel="stylesheet" type="text/css" href="index_htm_files/xr_text.css"/>
 <link href="css/myCustom.css" rel="stylesheet" type="text/css" />
 <script type="text/javascript" src="index_htm_files/roe.js"></script>
 <link href="css/FormCss_1.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="css/FormCss_2.css" />
<style type="text/css">
    .form-label{
        width:150px !important;
    }
    .form-label-left{
        width:150px !important;
    }
    .form-line{
        padding-top:12px;
        padding-bottom:12px;
    }
    .form-label-right{
        width:150px !important;
    }
    body, html{
        margin:0;
        padding:0;
        background:false;
    }

    .form-all{
        margin:0px auto;
        padding-top:20px;
        width:650px;
        color:#555 !important;
        font-family:'Lucida Grande';
        font-size:14px;
    }
    .form-radio-item label, .form-checkbox-item label, .form-grading-label, .form-header{
        color:#555;
    }

</style>

<script src="js/FormJS_1.JS" type="text/javascript"></script>
<script type="text/javascript">
   JotForm.init();
</script>
<link href="css/myCustom.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="xr_ap" id="xr_xr" style="width: 600px; height: 300px; top:0px; left:50%; margin-left: -680px;">
 <script type="text/javascript">var xr_xr=document.getElementById("xr_xr")</script>
<!--[if IE]><div class="xr_ap" id="xr_xri" style="width: 1360px; height: 800px; clip: rect(0px 1360px 800px 0px);"><![endif]-->
<!--[if !IE]>--><div class="xr_ap" id="xr_xri" style="width: 600px; height: 800px; clip: rect(0px, 600, 800px, 0px);"><!--<![endif]-->
 <img class="xr_ap" src="index_htm_files/9.png" alt="" title="" style="left: 1px; top: 4px; width: 1360px; height: 796px;"/>
 <div id="xr_xo0" class="xr_ap" style="left:0; top:0; width:600px; height:100px; visibility:hidden;">
 <a href="" onclick="return(false);">
 </a>
 </div>  
 <div class="edit_page">

<% 
	if(tsk.equalsIgnoreCase("deliver")){		
		int rowcount=getdat.getDatabase.getVehicleCompanyRow(company_id);
		DBObject[] vehicleData=getdat.getDatabase.getVehicleCompany(company_id, rowcount);
		String[] vehicle_id= new String[rowcount+1];
		if(vehicleData!=null){
			for(int i=1;i<=rowcount;i++){
				vehicle_id[i]=""+(String) vehicleData[i].get("stat");
				if(vehicle_id[i].equals("0")){
					vehicle_id[i]=(String) vehicleData[i].get("Vehicle_ID");
				}else{
					vehicle_id[i]=null;
				}
			}
		}else{
			System.out.println("no vehicle Data");
		}
		int rowcount2=getdat.getDatabase.getRowCount("driver", "Company_ID", company_id);
		DBObject[] driverData=getdat.getDatabase.getDriverCompany(company_id, rowcount2);
		String[] driver_id= new String[rowcount2+1];
		if(driverData!=null){
			for(int i=1;i<=rowcount2;i++){
				driver_id[i]=""+(String) driverData[i].get("stat");
				if(driver_id[i].equals("0")){
					driver_id[i]=(String) driverData[i].get("Driver_ID");
				}else{
					driver_id[i]=null;
				}
			}
		}else{
			System.out.println("no driver Data");
		}
		ses.setAttribute("task2","deliver");
		ses.setAttribute("dat_id",dat.get("Deliver_ID"));
		out.print("<form name =\"edited\" action=\"updatesuccess.tas\" method=\"post\">"
				+"<div>"
				+"<label style=\"font-weight:bold;\" id=\"deliver_id1\" >"
		        +"Deliver ID: "+dat.get("Deliver_ID")+""
		        +"</label> <br />"
				+"<input id=\"deliver_id\" name=\"deliver_id\"type=\"hidden\" value =\""+dat.get("Deliver_ID")+"\">"
				+"</div>"
				+"<div>"
				+"<label class=\"form-label-left\" id=\"driver_id1\" for=\"driver_id\">"
				+"Driver ID"
				+"</label>"
				+"<select id=\"driver_id\" name=\"driver_id\">");
		out.println("<option value=\""+dat.get("Driver_ID")+"\">"+dat.get("Driver_ID")+"</option>");
		for(int i=1;i<=rowcount2;i++){
			if(driver_id[i]==null){
				
			}else{
				out.println("<option value=\""+driver_id[i]+"\">"+driver_id[i]+"</option>");
				
			}
		}
				out.println("</select>"	
				+"</div>"
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"vehicle_id1\" for=\"vehicle_id\">"
				+"Vehicle ID"
				+"</label>"
				+"<select id=\"vehicle_id\" name=\"vehicle_id\">");
				out.println("<option value=\""+dat.get("Vehicle_ID")+"\">"+dat.get("Vehicle_ID")+"</option>");
				for(int i=1;i<=rowcount;i++){
					if(vehicle_id[i]==null){
						
					}else{
						out.println("<option value=\""+vehicle_id[i]+"\">"+vehicle_id[i]+"</option>");
						
					}
				}
				
				out.println("</select>"
				+"</div>"
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"stat1\" for=\"stat\">"
				+"Stat Code"
				+"</label>"
				+"<input id=\"stat\" name=\"stat\" type=\"text\" value =\""+dat.get("stat")+"\">"
				+"</div>"
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"origin1\" for=\"origin\">"
				+"Origin"
				+"</label>"
				+"<input id=\"origin\" name=\"origin\" type=\"text\" value =\""+dat.get("Origin")+"\"><br />"
				+"</div>"	
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"destination1\" for=\"destination\">"
				+"Destination"
				+"</label>"
				+"<input id=\"destination\" name=\"destination\" type=\"text\" value =\""+dat.get("Destination")+"\"><br />"
				+"</div>"
				+"<input type=\"submit\" value =\"OK\"><br />"
				+"</form>");
		
	}else if(tsk.equalsIgnoreCase("device")){
		ses.setAttribute("task2","device");
		ses.setAttribute("dat_id",dat.get("Device_ID"));
		out.print("<form name =\"edited\" action=\"updatesuccess.tas\" method=\"post\">"
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"device_id1\" for=\"device_id\">"
				+"Device ID: "+dat.get("Device_ID")+""
				+"</label>"
				+"<input id=\"device_id\" name=\"device_id\" type=\"hidden\" value =\""+dat.get("Device_ID")+"\"><br />"
						+"</div>"
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"vehicle_id1\" for=\"vehicle_id\">"
				+"Vehicle ID: "+dat.get("Vehicle_ID")+""
				+"</label>"
				+"<input id=\"vehicle_id\" name=\"vehicle_id\" type=\"hidden\" value =\""+dat.get("Vehicle_ID")+"\"><br />"
						+"</div>"
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"imei1\" for=\"imei\">"
				+"IMEI ID:"
				+"</label>"
				+"IMEI ID:<input id=\"imei\" name=\"imei\" type=\"text\" value =\""+dat.get("imei")+"\"><br />"
						+"</div>"
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"stat1\" for=\"stat1\">"
				+"Stat code:"
				+"</label>"
				+"<input id=\"stat\" name=\"stat\" type=\"text\" value =\""+dat.get("stat")+"\"><br />"
						+"</div>"
				+"<input type=\"submit\" value =\"OK\"><br />"
				+"</form>");
	}else if(tsk.equalsIgnoreCase("driver")){
		ses.setAttribute("task2","driver");
		ses.setAttribute("dat_id",dat.get("Driver_ID"));
		out.print("<form name =\"edited\" action=\"updatesuccess.tas\" method=\"post\">"
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"Driver_id1\" for=\"Driver_id\">"
				+"Driver ID: "+dat.get("Driver_ID")+""
				+"</label>"
				+"<input id=\"Driver_id\" name=\"Driver_id\" type=\"hidden\" value =\""+dat.get("Driver_ID")+"\"><br />"
						+"</div>"
				+"<div>" 
				+"<br />"
				+"<label class=\"form-label-left\" id=\"first_name1\" for=\"first_name\">"
				+"First Name:"
				+"</label>"
				+"<input id=\"first_name\" name=\"first_name\" type=\"text\" value =\""+dat.get("First_name")+"\"><br />"
				+"</div>"				
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"last_name1\" for=\"last_name\">"
				+"Last Name:"
				+"</label>"
				+"<input id=\"last_name\" name=\"last_name\" type=\"text\" value =\""+dat.get("Last_name")+"\"><br />"
						+"</div>"				
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"year1\" >"
				+"Date of Birth"
				+"</label>"
				+"<br />"
				+"Year:<input id=\"year\" name=\"year\" type=\"text\" value =\""+dat.get("Year")+"\" style=\"width:70px\">"

				+"Month:<input name=\"month\" type=\"text\" value =\""+dat.get("Month")+"\" style=\"width:70px\">"
	
				+"Day:<input name=\"day\" type=\"text\" value =\""+dat.get("Day")+"\" style=\"width:70px\"><br />"
						+"</div>"				
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"sex1\" for=\"sex\">"
				+"Sex:"
				+"</label>"
				+"<input id=\"sex\" name=\"sex\" type=\"text\" value =\""+dat.get("Sex")+"\"><br />"
						+"</div>"				
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"stat1\" for=\"stat\">"
				+"Stat Code"
				+"</label>"
				+"<input id=\"stat\" name=\"stat\" type=\"text\" value =\""+dat.get("stat")+"\"><br />"
						+"</div>"	
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"province1\" for=\"province\">"
				+"Province:"
				+"</label>"
				+"<input id=\"province\" name=\"province\" type=\"text\" value =\""+dat.get("Province")+"\"><br />"
						+"</div>"				
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"city1\" for=\"city\">"
				+"City:"
				+"</label>"
				+"<input id=\"city\" name=\"city\" type=\"text\" value =\""+dat.get("City")+"\"><br />"
						+"</div>"				
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"address1\" for=\"address\">"
				+"Address:"
				+"</label>"
				+"<textarea rows=\"5\" cols=\"40\" id=\"address\" name=\"address\" type=\"text\" value =\""+dat.get("Address")+"\">"+dat.get("Address")+"</textarea><br />"
						+"</div>"								
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"email1\" for=\"email\">"
				+"E-Mail:"
				+"</label>"
				+"<input id=\"email\" name=\"email\" type=\"text\" value =\""+dat.get("E-Mail")+"\"size=\"50\"><br />"
						+"</div>"				
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"phone1\" for=\"phone\">"
				+"Phone:"
				+"</label>"
				+"<input id=\"phone\" name=\"phone\" type=\"text\" value =\""+dat.get("Phone")+"\"><br />"
						+"</div>"
				+"<input type=\"submit\" value =\"OK\"><br />"
				+"</form>");
	}else if(tsk.equalsIgnoreCase("vehicle")){
		BasicDBObject queryde = new BasicDBObject();
		queryde.put("Company_ID", company_id);		
		int rowcount=getdat.getDatabase.getRowCount("device", queryde);		
		DBObject[] deviceData=getdat.getDatabase.getCollData("device", queryde, rowcount);
		String[] device_id= new String[rowcount+1];
		if(deviceData!=null){
			for(int i=1;i<=rowcount;i++){
				device_id[i]=""+(String) deviceData[i].get("stat");
				if(device_id[i].equals("0")){
					device_id[i]=(String) deviceData[i].get("Device_ID");
				}else{
					device_id[i]=null;
				}
			}
		}else{
			System.out.println("no vehicle Data");
		}
		ses.setAttribute("task2","vehicle");
		ses.setAttribute("dat_id",dat.get("Vehicle_ID"));
		out.print("<form name =\"edited\" action=\"updatesuccess.tas\" method=\"post\">"
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"vehicle_id1\" for=\"vehicle_id\">"
				+"Vehicle ID: "+dat.get("Vehicle_ID")+""
				+"</label>"
				+"<input id=\"vehicle_id\" name=\"vehicle_id\" type=\"hidden\" value =\""+dat.get("Vehicle_ID")+"\"><br />"
						+"</div>"				
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"registerid1\" for=\"registerid\">"
				+"RegisterID:"
				+"</label>"
				+"<input id=\"registerid\" name=\"registerid\" type=\"text\" value =\""+dat.get("RegisterID")+"\"><br />"
						+"</div>"				
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"device_id1\" for=\"device_id\">"
				+"Device ID:"
				+"</label>"
				+"<select id=\"device_id\" name=\"device_id\">");
					out.println("<option value=\""+dat.get("Device_ID")+"\">"+dat.get("Device_ID")+"</option>");
					for(int i=1;i<=rowcount;i++){
						if(device_id[i]==null){
				
						}else{
							out.println("<option value=\""+device_id[i]+"\">"+device_id[i]+"</option>");
				
						}
					}
		out.println("</select>"	
						+"</div>"	
								+"<div>"
								+"<br />"
								+"<label class=\"form-label-left\" id=\"year1\" >"
								+"Year of purchase"
								+"</label>"
								+"<br />"
								+"Year:<input id=\"year\" name=\"year\" type=\"text\" value =\""+dat.get("Year")+"\" style=\"width:70px\">"

								+"Month:<input name=\"month\" type=\"text\" value =\""+dat.get("Month")+"\" style=\"width:70px\">"
					
								+"Day:<input name=\"day\" type=\"text\" value =\""+dat.get("Day")+"\" style=\"width:70px\"><br />"
										+"</div>"				
								+"<div>"
								+"<br />"								
								+"<label class=\"form-label-left\" id=\"gear1\" for=\"gear\">"
								+"Gear:"
								+"</label>"
								+"<input id=\"gear\" name=\"gear\" type=\"text\" value =\""+dat.get("Gear")+"\"><br />"
										+"</div>"				
								+"<div>"
								+"<br />"
								+"<label class=\"form-label-left\" id=\"brand1\" for=\"brand\">"
								+"Brand:"
								+"</label>"
								+"<input id=\"brand\" name=\"brand\" type=\"text\" value =\""+dat.get("Brand")+"\"><br />"
										+"</div>"				
								+"<div>"
								+"<br />"
								+"<label class=\"form-label-left\" id=\"model1\" for=\"model\">"
								+"Model:"
								+"</label>"
								+"<input id=\"model\" name=\"model\" type=\"text\" value =\""+dat.get("Model")+"\"><br />"
										+"</div>"				
								+"<div>"
								+"<br />"
								+"<label class=\"form-label-left\" id=\"color1\" for=\"color\">"
								+"Color:"
								+"</label>"
								+"<input id=\"color\" name=\"color\" type=\"text\" value =\""+dat.get("Color")+"\"><br />"
										+"</div>"				
								+"<div>"
								+"<br />"
								+"<label class=\"form-label-left\" id=\"description1\" for=\"description\">"
								+"Description:"
								+"</label>"
								+"<textarea rows=\"5\" cols=\"40\" id=\"description\" name=\"description\" type=\"text\" value =\""+dat.get("Description")+"\">"+dat.get("Description")+"</textarea><br />"
										+"</div>"								
								+"<div>"
								+"<br />"
								+"<label class=\"form-label-left\" id=\"engine1\" for=\"engine\">"
								+"Engine:"
								+"</label>"
								+"<input id=\"engine\" name=\"engine\" type=\"text\" value =\""+dat.get("Engine")+"\"><br />"
										+"</div>"				
								
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"stat1\" for=\"stat\">"
				+"stat code:"
				+"</label>"
				+"<input id=\"stat\" name=\"stat\" type=\"text\" value =\""+dat.get("stat")+"\"><br />"
						+"</div>"				
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"type1\" for=\"type\">"
				+"Type:"
				+"</label>"
				+"<input id=\"type\" name=\"type\" type=\"text\" value =\""+dat.get("Type")+"\"><br />"
						+"</div>"
				+"<input type=\"submit\" value =\"OK\"><br />"
				+"</form>");
	}else if(tsk.equalsIgnoreCase("product")){
		ses.setAttribute("task2","product");
		ses.setAttribute("dat_id",dat.get("Product_ID"));
		out.print("<form name =\"edited\" action=\"updatesuccess.tas\" method=\"post\">"
				+"<input name=\"deliver_id\" type=\"hidden\" value =\""+dat.get("Deliver_ID")+"\">"
						+"<div>"
						+"<br />"
						+"<label class=\"form-label-left\" id=\"product_id1\" for=\"product_id\">"
						+"Product ID:"
						+"</label>"
				+"<input id=\"product_id\" name=\"product_id\" type=\"text\" value =\""+dat.get("Product_ID")+"\"><br />"
						+"</div>"				
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"product_name1\" for=\"product_name\">"
				+"Product Name:"
				+"</label>"
				+"<input id=\"product_name\" name=\"product_name\" type=\"text\" value =\""+dat.get("Product_Name")+"\"><br />"
						+"</div>"				
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"description1\" for=\"description\">"
				+"Description:"
				+"</label>"
				+"<textarea rows=\"5\" cols=\"40\" id=\"description\" name=\"description\" type=\"text\" value =\""+dat.get("Description")+"\">"+dat.get("Description")+"</textarea><br />"
						+"</div>"				
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"price1\" for=\"price\">"
				+"Price:"
				+"</label>"
				+"<input id=\"price\" name=\"price\" type=\"text\" value =\""+dat.get("Price")+"\"><br />"
						+"</div>"				
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"amount1\" for=\"amount\">"
				+"Amount:"
				+"</label>"
				+"<input id=\"amount\" name=\"amount\" type=\"text\" value =\""+dat.get("Amount")+"\"><br />"
						+"</div>"
				+"<input type=\"submit\" value =\"OK\"><br />"
				+"</form>");
	}else if(tsk.equalsIgnoreCase("customer")){
		ses.setAttribute("task2","customer");
		ses.setAttribute("dat_id",dat.get("Customer_ID"));
		out.print("<form name =\"edited\" action=\"updatesuccess.tas\" method=\"post\">"
				+"<input name=\"customer_id\" type=\"hidden\" value =\""+dat.get("Customer_ID")+"\">"
				+"<div>"
				+"<br />"
				+"<label class=\"form-label-left\" id=\"customer_name1\" for=\"customer_name\">"
				+"Customer Name:"
				+"</label>"
				+"<input id=\"customer_name\" name=\"customer_name\" type=\"text\" value =\""+dat.get("Customer_name")+"\"><br />"
				+"</div>"
						+"<div>"
						+"<br />"
						+"<label class=\"form-label-left\" id=\"year1\" >"
						+"Date of Birth"
						+"</label>"
						+"<br />"
						+"Year:<input id=\"year\" name=\"year\" type=\"text\" value =\""+dat.get("Year")+"\" style=\"width:70px\">"

						+"Month:<input name=\"month\" type=\"text\" value =\""+dat.get("Month")+"\" style=\"width:70px\">"
			
						+"Day:<input name=\"day\" type=\"text\" value =\""+dat.get("Day")+"\" style=\"width:70px\"><br />"
								+"</div>"				
						+"<div>"
						+"<br />"
						+"<label class=\"form-label-left\" id=\"sex1\" for=\"sex\">"
						+"Sex:"
						+"</label>"
						+"<input id=\"sex\" name=\"sex\" type=\"text\" value =\""+dat.get("Sex")+"\"><br />"
								+"</div>"				
						+"<div>"
						+"<br />"
						+"<label class=\"form-label-left\" id=\"stat1\" for=\"stat\">"
						+"Stat Code"
						+"</label>"
						+"<input id=\"stat\" name=\"stat\" type=\"text\" value =\""+dat.get("stat")+"\"><br />"
								+"</div>"	
						+"<div>"
						+"<br />"
						+"<label class=\"form-label-left\" id=\"country1\" for=\"country\">"
						+"Country:"
						+"</label>"
						+"<input id=\"country\" name=\"country\" type=\"text\" value =\""+dat.get("Country")+"\"><br />"
								+"</div>"			
						+"<div>"
						+"<br />"
						+"<label class=\"form-label-left\" id=\"province1\" for=\"province\">"
						+"Province:"
						+"</label>"
						+"<input id=\"province\" name=\"province\" type=\"text\" value =\""+dat.get("Province")+"\"><br />"
								+"</div>"				
						+"<div>"
						+"<br />"
						+"<label class=\"form-label-left\" id=\"city1\" for=\"city\">"
						+"City:"
						+"</label>"
						+"<input id=\"city\" name=\"city\" type=\"text\" value =\""+dat.get("City")+"\"><br />"
								+"</div>"				
						+"<div>"
						+"<br />"
						+"<label class=\"form-label-left\" id=\"address1\" for=\"address\">"
						+"Address:"
						+"</label>"
						+"<textarea rows=\"5\" cols=\"40\" id=\"address\" name=\"address\" type=\"text\" value =\""+dat.get("Address")+"\">"+dat.get("Address")+"</textarea><br />"
								+"</div>"								
						+"<div>"
						+"<br />"
						+"<label class=\"form-label-left\" id=\"email1\" for=\"email\">"
						+"E-Mail:"
						+"</label>"
						+"<input id=\"email\" name=\"email\" type=\"text\" value =\""+dat.get("E-Mail")+"\" size=\"50\"><br />"
								+"</div>"				
						+"<div>"
						+"<br />"
						+"<label class=\"form-label-left\" id=\"phone1\" for=\"phone\">"
						+"Phone:"
						+"</label>"
						+"<input id=\"phone\" name=\"phone\" type=\"text\" value =\""+dat.get("Phone")+"\"><br />"
								+"</div>"
				+"<input type=\"submit\" value =\"OK\"><br />"
				+"</form>");
	}else{
		out.print("<br /> something Wrong");
	}
	
%>
 </div>
 <div id="xr_xd0"> 
 </div>
</div>
</div>
<!--[if lt IE 7]><script type="text/javascript" src="index_htm_files/png.js"></script><![endif]-->
<script type="text/javascript">xr_aeh()</script>
</body>
</html>