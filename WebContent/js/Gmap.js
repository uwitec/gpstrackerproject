var numMarker=1;
var map;
var marker = new Array();
var timestamp = new Array();
var path;
var row_new=0;
var row_old=0;
var veh_id;
var rMarker;
var rPath;
function initialize_GMAP(vehicle_id) {
	
        var mapOptions = {
          center: new google.maps.LatLng(-34.397, 150.644),
          zoom: 8,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map_canvas"),
            mapOptions);
        veh_id=vehicle_id;
        rMarker= new google.maps.Marker({  	     
  	      map: map,
  	      title:"Hello World!",
  	      icon:"img/rCar2.PNG"
  	  });
        google.maps.event.addListener(rMarker, 'mouseover', function(){markerClick(rMarker,numMarker-1);});
        rPath =  new google.maps.Polyline({   
        	map: map,
            strokeColor: "#FF0000",
            strokeOpacity: 1.0,
            strokeWeight: 2
          });
        path = rPath.getPath();
        //updateMap();
        setInterval(updateMap,1000);
        //var timer = setTimeout(updateMap, 1000);
 }
function updatMarker(lat,lon){
	if(numMarker==1){
		var myLatlng = new google.maps.LatLng(lat,lon);
		rMarker.setPosition(myLatlng);
		marker[numMarker] = new google.maps.Marker({
			position: myLatlng,
			map: map,
			title:"Hello World!",
			icon:"img/Flag1RightGreen2.png"
		});	
		path.push(myLatlng);
		map.setCenter(myLatlng);
		var num15=numMarker;
		google.maps.event.addListener(marker[num15], 'mouseover', function(){markerClick(marker[num15],num15);});
		numMarker=numMarker+1;
		
	}else{
		var num15=numMarker;
		var myLatlng = new google.maps.LatLng(lat,lon);
		rMarker.setPosition(myLatlng);
		marker[numMarker] = new google.maps.Marker({
			position: myLatlng,
			map: map,
			title:"Hello World!",
			//icon:"img/testico.PNG"
		});	
		path.push(myLatlng);
		map.setCenter(myLatlng);
		google.maps.event.addListener(marker[num15], 'mouseover', function(){
			markerClick(marker[num15],num15);
			});
		markerClick(marker[num15],num15);
		numMarker=numMarker+1;
	}
}
function updateMap(){
	var x;
	var xmlhttp;
	var lat=new Array();
	var lon=new Array();	
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		    {
			  xmlDoc=xmlhttp.responseXML;
			    txt="";
			    x=xmlDoc.getElementsByTagName("ROWOLD");
			    row_new=parseInt(x[0].childNodes[0].nodeValue);
			    row_sum=row_new-row_old;
			    x2=xmlDoc.getElementsByTagName("LAT");
			    x3=xmlDoc.getElementsByTagName("LON");
			    x4=xmlDoc.getElementsByTagName("TIME");
			    if(row_old<row_new){
			    	for(var i=row_old;i<row_new;i++){
			    		//x=xmlDoc.getElementsByTagName("LAT");
			    	 	lat[i]=x2[i].childNodes[0].nodeValue;
			    	 	//x=xmlDoc.getElementsByTagName("LON");
			    		lon[i]=x3[i].childNodes[0].nodeValue;
			    		timestamp[i+1]=x4[i].childNodes[0].nodeValue;
			    		updatMarker(lat[i],lon[i]);		    		
					    
			    	}  
			    }			   
			    document.getElementById("row_old").innerHTML=row_old+"";
			    document.getElementById("row_new").innerHTML=row_new+"";
			    row_old=row_new;			   
			    //setTimeout(updateMap, 3000);
			   //setTimeout("updateMap('"+row_old+"','"+row_new+"')", 1000);
			   
		  }
	  };
	  xmlhttp.open("POST","utilJS",true);		
	  xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	  xmlhttp.send("param=upGmap&vehid="+veh_id+"&row_old="+row_old+"");
	  //setTimeout(updateMap, 3000);
	  //setTimeout("updateMap('"+row_old+"','"+row_new+"','"+veh_id+"')", 1000);
}
function markerClick(tMarker,numM){
	for(var t =1;t<numMarker;t++){
		marker[t].setAnimation(null);
		
	}
	rMarker.setAnimation(null);
	 if (tMarker.getAnimation() != null) {
		 tMarker.setAnimation(null);
		  } else {
			  tMarker.setAnimation(google.maps.Animation.BOUNCE);
		  	
			  var xmlhttp;	
			  var x
			  if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
			  }
			  else
			  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			  xmlhttp.onreadystatechange=function()
			  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				  {
					  xmlDoc=xmlhttp.responseXML;
			    
					  x=xmlDoc.getElementsByTagName("DELIVERID");
					  document.getElementById("la1").innerHTML=x[0].childNodes[0].nodeValue+"";
					  x=xmlDoc.getElementsByTagName("COMMAND");
					  document.getElementById("la2").innerHTML=x[0].childNodes[0].nodeValue+"";
					  x=xmlDoc.getElementsByTagName("CODE");
					  document.getElementById("la3").innerHTML=x[0].childNodes[0].nodeValue+"";
					  x=xmlDoc.getElementsByTagName("LAT");
					  document.getElementById("la4").innerHTML=x[0].childNodes[0].nodeValue+"";
					  x=xmlDoc.getElementsByTagName("LON");
					  document.getElementById("la5").innerHTML=x[0].childNodes[0].nodeValue+"";
					  x=xmlDoc.getElementsByTagName("TIME");
					  document.getElementById("la6").innerHTML=x[0].childNodes[0].nodeValue+"";
					  x=xmlDoc.getElementsByTagName("INDI");
					  document.getElementById("la7").innerHTML=x[0].childNodes[0].nodeValue+"";
					  x=xmlDoc.getElementsByTagName("SA");
					  document.getElementById("la8").innerHTML=x[0].childNodes[0].nodeValue+"";
					  x=xmlDoc.getElementsByTagName("SIGNAL");
					  document.getElementById("la9").innerHTML=x[0].childNodes[0].nodeValue+"";
					  x=xmlDoc.getElementsByTagName("SPEED");
					  document.getElementById("la10").innerHTML=x[0].childNodes[0].nodeValue+"";
					  x=xmlDoc.getElementsByTagName("HEAD");
					  document.getElementById("la11").innerHTML=x[0].childNodes[0].nodeValue+"";
					  x=xmlDoc.getElementsByTagName("ALTITUDE");
					  document.getElementById("la12").innerHTML=x[0].childNodes[0].nodeValue+"";
					  x=xmlDoc.getElementsByTagName("MILEAGE");
					  document.getElementById("la13").innerHTML=x[0].childNodes[0].nodeValue+"";
			    	    	
			    
			     
				  }
			  };
			  xmlhttp.open("POST","utilJS",true);		
			  xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			  xmlhttp.send("param=Emarker&time="+timestamp[numM]+"&row_old="+row_old+"");
		  }
	  
}