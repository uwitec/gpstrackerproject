<%@page import="insDB.PutDB"%>
<%@page import="getdat.getDatabase"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="java.util.*,com.mongodb.*"%>
<%
	HttpSession ses = request.getSession();
	String usn = (String) ses.getAttribute("usn");
	String pwd = (String) ses.getAttribute("pwd");
	String user_stat = (String) ses.getAttribute("user_stat");
	if(user_stat.equals("")||user_stat==null){
		
		String wr="Wrong ID or Password";
		request.setAttribute("wrong", wr);
		RequestDispatcher view = request.getRequestDispatcher("start.jsp?note="+wr+"");
		view.forward(request, response);
					    
		}
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
<%
	usn=(String) ses.getAttribute("usn");
	pwd=(String) ses.getAttribute("pwd");
	user_stat=(String) ses.getAttribute("user_stat");
	String company_id=(String) ses.getAttribute("company_id");
	HttpServletRequest req=request;
	boolean dp=getDatabase.checkDupdata("manufacture", "Manufacture_ID", req.getParameter("p_id"));
	if(req.getParameter("p_name").equalsIgnoreCase("")){
		out.println("ไม่มีชื่อสินค้าที่จะผลิต");
	}else if(req.getParameter("p_count").equalsIgnoreCase("")){
		out.println("ไม่มีจำนวนสินค้าที่จะผลิต");
	}else if(req.getParameter("p_id").equalsIgnoreCase("")){
		out.println("ไม่มีรหัสผลิตสินค้า");
	}else if(dp){
		out.println("รหัสผลิตสินค้าซ้ำ");
	}
	else{
		boolean f1=false;
		f1=PutDB.ins_manufacture(company_id, req.getParameter("p_id"), req.getParameter("p_name"),req.getParameter("p_count"),req.getParameter("order_day")+"",req.getParameter("order_month")+"",req.getParameter("order_year")+"",req.getParameter("in_day")+"",req.getParameter("in_month")+"",req.getParameter("in_year")+"",req.getParameter("p_warehouse")+"");
		if(f1){
			int nm=Integer.parseInt(req.getParameter("n_list"));
			boolean f2=false;
			for(int i=1;i<=nm;i++){
				    f2=PutDB.ins_Material(company_id, req.getParameter("p_id"), req.getParameter("m_num_"+nm),
					req.getParameter("m_name_"+nm), req.getParameter("m_des_"+nm),
					req.getParameter("m_count_"+nm), req.getParameter("m_w_"+nm),
					req.getParameter("m_v_"+nm), req.getParameter("m_price_"+nm),req.getParameter("m_seller_"+nm));
			}
			if(f2){
				out.println("Add manufacture เสร็จ");
			}else{
				out.println("Add material Failed");
			}
		}else{
			out.println("Add manufacture Failed");
		}
	}
%>

</body>
</html>