<%@page import="getdat.getDatabase"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="java.util.*,com.mongodb.*"%>
<%
	HttpSession ses = request.getSession();
	String usn = (String) ses.getAttribute("usn");
	String pwd = (String) ses.getAttribute("pwd");
	String user_stat = (String) ses.getAttribute("user_stat");
	if(user_stat.equals("")||user_stat==null){
		
		String wr="Wrong ID or Password";
		request.setAttribute("wrong", wr);
		RequestDispatcher view = request.getRequestDispatcher("start.jsp?note="+wr+"");
		view.forward(request, response);
					    
		}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript" charset="UTF-8" src="./js/util.js"></script>

</head>
<body>
<div style="position: absolute; left:0%; top:0%;width:40%; height: 70%;">
<form id="form_1" method="post" action="./addraw_success.php">
<label style="position: absolute; left:20px; top:20px">รหัสการผลิตสินค้า</label>
<input id="p_id" name="p_id" type="text" style="position: absolute; left:200px; top:20px">
<label style="position: absolute; left:20px; top:60px">ชื่อสินค้าที่จะผลิต</label>
<input id="p_name" name="p_name" type="text" style="position: absolute; left:200px; top:60px">
<label style="position: absolute; left:20px; top:100px">จำนวนที่จะผลิต</label>
<input id="p_count" name="p_count" type="text" style="position: absolute; left:200px; top:100px" onchange="changeby_p_count()">
<label style="position: absolute; left:20px; top:140px">เก็บที่คลังสินค้า</label>
<select id="p_warehouse" name="p_warehouse" style="position: absolute; left:200px; top:140px;width:150px">

<option value="wherehouse">wherehouse1</option>	

</select>
<label style="position: absolute; left:20px; top:180px">วันที่สั่งผลิต</label>
<input id="order_day" name="order_day" type="text" style="position: absolute; left:200px; top:180px;width:50px" >
<input id="order_month" name="order_month" type="text" style="position: absolute; left:260px; top:180px;width:50px" >
<input id="order_year" name="order_year" type="text" style="position: absolute; left:320px; top:180px;width:50px">
<label style="position: absolute; left:20px; top:220px">ผลิตภายในกำหนด</label>
<input id="in_day" name="in_day" type="text" style="position: absolute; left:200px; top:220px;width:50px" >
<input id="in_month" name="in_month" type="text" style="position: absolute; left:260px; top:220px;width:50px" >
<input id="in_year" name="in_year" type="text" style="position: absolute; left:320px; top:220px;width:50px">
<input type="button" value="เพิ่มรายการผลิต" onclick="submit_form()" style="position: absolute; left:200px; top:300px;"/>
<div id="m_holder" style="position: absolute; left:10px; top:140px;width:100%; height: 40%;z-index:-10;">
<input id="n_list" name="n_list" type="hidden" value="0">
</div>
</form>
</div>
<div style="position: absolute; left:41%; top:0%;width:40%; height: 70%;">
<label style="position: absolute; left:20px; top:20px">หมายเลขส่วนประกอบ</label>
<input id="m_num" name="m_num" type="text" style="position: absolute; left:200px; top:20px">
<label style="position: absolute; left:20px; top:60px">วัสดุ</label>
<input id="m_name" name="m_name" type="text" style="position: absolute; left:200px; top:60px">
<label style="position: absolute; left:20px; top:100px">รายละเอียดวัสดุ</label>
<input id="m_des" name="m_des" type="text" style="position: absolute; left:200px; top:100px;width:300px">
<label style="position: absolute; left:20px; top:140px">จำนวนต่อหนึ่งหน่วยผลิต</label>
<input id="m_count" name="m_count" type="text" style="position: absolute; left:200px; top:140px">
<label style="position: absolute; left:20px; top:180px">น้ำหนักต่อชิ้น </label> <label style="position: absolute; left:400px; top:180px">Kg</label>
<input id="m_w" name="m_w" type="text" style="position: absolute; left:200px; top:180px">
<label style="position: absolute; left:20px; top:220px">ปริมาตรต่อชิ้น </label> <label style="position: absolute; left:400px; top:220px;width:200px">ลูกบาศเมตร</label>
<input id="m_v" name="m_v" type="text" style="position: absolute; left:200px; top:220px">
<label style="position: absolute; left:20px; top:260px">ราคาต่อชิ้น </label> <label style="position: absolute; left:400px; top:260px">บาท</label>
<input id="m_price" name="m_price" type="text" style="position: absolute; left:200px; top:260px">
<label style="position: absolute; left:20px; top:300px">ผู้ขาย</label>
<select id="m_seller" name="m_seller" style="position: absolute; left:200px; top:300px;width:200px">
<%
for(int i=1;i<=4;i++){
	out.println("<option value=\"test11\">test11</option>");
	
	}
%>
</select>
<input type="button" value="เพิ่มส่วนประกอบ" onclick="add_m()" style="position: absolute; left:200px; top:340px;"/>
</div>
<div style="position: absolute;overflow:scroll; left:0%; top:380px;width:90%; height: 50%;">
<table id="table_dat" border="1">
  <tr>
    <th>หมายเลขส่วนประกอบ</th>
    <th>วัสดุ</th>
    <th>รายละเอียดวัสดุ</th>
    <th>จำนวนต่อหนึ่งหน่วยผลิต</th>
    <th>น้ำหนักต่อชิ้น</th>
    <th>ปริมาตรต่อชิ้น</th>
    <th>ราคาต่อชิ้น</th>
    <th>จำนวนรวม</th>
    <th>น้ำหนักรวม</th>
    <th>ปริมาตรรวม</th>
    <th>ราคารวม</th>
    <th>ผู้ขาย</th>
  </tr>  
</table>
</div>
</body>
</html>