<%@page import="getdat.getDatabase"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="java.util.*,com.mongodb.*"%>
<%
	HttpSession ses = request.getSession();
	String usn = (String) ses.getAttribute("usn");
	String pwd = (String) ses.getAttribute("pwd");
	String user_stat = (String) ses.getAttribute("user_stat");
	if(user_stat.equals("")||user_stat==null){
		
		String wr="Wrong ID or Password";
		request.setAttribute("wrong", wr);
		RequestDispatcher view = request.getRequestDispatcher("start.jsp?note="+wr+"");
		view.forward(request, response);
					    
		}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<!-- drop down script -->
<link rel="stylesheet" type="text/css" href="./js/ddsmoothmenu.css" />
<link rel="stylesheet" type="text/css" href="./js/ddsmoothmenu-v.css" />

<script type="text/javascript" src="./js/jquery_lib.1.3.2.js"></script>
<script type="text/javascript" src="./js/ddsmoothmenu.js">

/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script type="text/javascript">

ddsmoothmenu.init({
	mainmenuid: "print1", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	customtheme: ["#F0FFFF", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
});


</script>
<!-- end drop script -->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript" charset="UTF-8" src="./js/util.js"></script>
<script type="text/javascript" charset="UTF-8" src="./js/vendor.js"></script>
<script type="text/javascript" charset="UTF-8" src="./js/navigator_bar.js"></script>

</head>
<body>
<div style="position: absolute; left:300px; top:0px;width:60%; height:30;">
<input type="button" value="new" onclick="window.open('./add_vendor.jsp','iframe_1')" style="position: absolute; left:10px; top:10px;"/>
<input type="button" value="save" onclick="window.open('./add_vendor.jsp','iframe_1')" style="position: absolute; left:50px; top:10px;"/>
<div id="print1" class="ddsmoothmenu" style="position: absolute; left:100px; top:10px;">
<ul>
<li><a href="#">พิมพ์เอกสาร</a>
  <ul>
  <li><a href="#">Purchase Order</a></li>
  <li><a href="#">Receiving note</a></li>
  <li><a href="#">Purchase Invoice</a></li>
  <li><a href="#">Purchase Return</a></li>  
  </ul>
</li>
</ul>
</div>

</div>
<div style="position: absolute; left:0px; top:0px;width:290px; height:90%;">
<div style="position: absolute; left:0px; top:0px;width:290px; height:40%;">
<label style="position: absolute; left:20px; top:20px">Search</label>
<select id="p_warehouse" name="p_warehouse" style="position: absolute; left:20px; top:60px;width:100px">

<option value="name">ชื่อ</option>
<option value="phone">โทรศัพ</option>
<option value="fax">แฟค</option>	
<option value="email">อีเมล</option>
<option value="province">จังหวัด</option>
<option value="country">ประเทศ</option>
<option value="web">Website</option>	
<option value="currency">สกุลเงิน</option>	

</select>
<select id="p_warehouse" name="p_warehouse" style="position: absolute; left:20px; top:100px;width:100px">

<option value="name">ชื่อ</option>
<option value="phone">โทรศัพ</option>
<option value="fax">แฟค</option>	
<option value="email">อีเมล</option>
<option value="province">จังหวัด</option>
<option value="country">ประเทศ</option>
<option value="web">Website</option>	
<option value="currency">สกุลเงิน</option>		

</select>
<select id="p_warehouse" name="p_warehouse" style="position: absolute; left:20px; top:140px;width:100px">

<option value="name">ชื่อ</option>
<option value="phone">โทรศัพ</option>
<option value="fax">แฟค</option>	
<option value="email">อีเมล</option>
<option value="province">จังหวัด</option>
<option value="country">ประเทศ</option>
<option value="web">Website</option>	
<option value="currency">สกุลเงิน</option>		

</select>
<input id="p_id" name="p_id" type="text" style="position: absolute; left:130px; top:60px">
<input id="p_id" name="p_id" type="text" style="position: absolute; left:130px; top:100px">
<input id="p_id" name="p_id" type="text" style="position: absolute; left:130px; top:140px">
<input type="button" value="Search" onclick="window.open('./vendor_history.jsp','_self')" style="position: absolute; left:50px; top:180px;"/>
</div>
<div style="position: absolute; left:0px; top:220px;width:290px; height:50%;">
<label style="position: absolute; left:20px; top:20px">เลขที่ใบสั่งซื้อ</label>
<div style="position: absolute; left:20px; top:40px">
<table id="com_dat" border="1" style="width:100%;">

</table>
</div>
</div>
</div>
<div style="position: absolute; left:300px; top:30px;width:80%; height: 90%;visibility: visible;">

<input type="button" value="ใบสั่งซื้อ" onclick="showdiv('puchese_order')" style="position: absolute; left:0px; top:10px;"/>
<input type="button" value="รับของ" onclick="showdiv('puchese_recive')" style="position: absolute; left:100px; top:10px;"/>
<input type="button" value="ชำระเงิน" onclick="showdiv('puchese_payment')" style="position: absolute; left:200px; top:10px;"/>
<input type="button" value="ของส่งคืน" onclick="showdiv('puchese_return')" style="position: absolute; left:300px; top:10px;"/>
<input type="button" value="ขนสินค้าคืน" onclick="showdiv('puchese_unstock')" style="position: absolute; left:400px; top:10px;"/>
<label style="position: absolute; left:0px; top:40px">ผู้ขาย</label>
<input id="p_id" name="p_id" type="text" style="position: absolute; left:100px; top:40px">
<label style="position: absolute; left:0px; top:80px">ติดต่อ</label>
<input id="p_id" name="p_id" type="text" style="position: absolute; left:100px; top:80px">
<label style="position: absolute; left:0px; top:120px">โทรศัพ</label>
<input id="p_id" name="p_id" type="text" style="position: absolute; left:100px; top:120px">
<label style="position: absolute; left:300px; top:40px">ที่อยู่</label>
<select id="p_warehouse" name="p_warehouse" style="position: absolute; left:400px; top:40px;width:100px">
<option value="name">-------</option>
</select>
<textarea disabled rows="3" cols="40" style="position: absolute; left:300px; top:60px"></textarea>
<label style="position: absolute; left:300px; top:120px">ส่งคลังสินค้า</label>
<select id="p_warehouse" name="p_warehouse" style="position: absolute; left:400px; top:120px;width:100px">
<option value="name">---------</option>
</select>
<label style="position: absolute; left:600px; top:40px">เลขที่ใบสั่งซื้อ</label>
<input id="p_id" name="p_id" type="text" style="position: absolute; left:700px; top:40px">
<label style="position: absolute; left:600px; top:80px">วันที่</label>
<input id="p_id" name="p_id" type="text" style="position: absolute; left:700px; top:80px">
<label style="position: absolute; left:600px; top:120px">สถานะ</label>
<input disabled id="p_id" name="p_id" type="text" style="position: absolute; left:700px; top:120px">

<div id="puchese_order" style="position: absolute; left:0px; top:160px;width:90%; height: 65%;visibility: visible;">
<div style="position: absolute;overflow:scroll; left:0px; top:0px;width:100%; height: 60%;">
<table id="table_dat1" border="1" style="width:100%;">
<tr>
<th style="width:200px;">ชื่อสินค้า</th>
<th style="width:200px;">รหัสสินค้าของผู้ขาย</th>
<th style="width:200px;">จำนวน</th>
<th style="width:200px;">ราคาต่อหน่วย</th>
<th style="width:200px;">ส่วนลด</th>
<th style="width:200px;">ราคารวม</th>
</tr>
</table>
</div>
<div style="position: absolute; left:0px; top:60.01%;width:100%; height: 40%;">
<label style="position: absolute; left:0px; top:0px">ภาษี</label>
<input id="p_id" name="p_id" type="text" style="position: absolute; left:150px; top:0px">
<label style="position: absolute; left:0px; top:40px">ค่าใช้จ่ายอื่นๆ</label>
<input id="p_id" name="p_id" type="text" style="position: absolute; left:150px; top:40px">
<label style="position: absolute; left:0px; top:80px">สกุลเงิน</label>
<select id="p_warehouse" name="p_warehouse" style="position: absolute; left:150px; top:80px;width:150px">

<option value="EU Euro (€)">EU Euro (€)</option>
<option value="Japanese Yen  (¥)">Japanese Yen  (¥)</option>
<option value="Thai Baht (฿)">Thai Baht (฿)</option>	
<option value="US Dollar  ($)">US Dollar  ($)</option>	

</select>
<label style="position: absolute; left:350px; top:22px">หมายเหตุ</label>
<textarea rows="4" cols="50" style="position: absolute; left:350px; top:40px"></textarea>
<label style="position: absolute; left:780px; top:22px">ราคารวม</label>
<label style="position: absolute; left:780px; top:40px">0</label>
<input type="button" value="ทำรายการเดิมไหม่" onclick="" style="position: absolute; left:350px; top:170px;"/>
</div>
</div>

<div id="puchese_recive" style="position: absolute; left:0px; top:160px;width:90%; height: 65%;visibility: hidden;">
<div style="position: absolute;overflow:scroll; left:0px; top:0px;width:100%; height: 60%;">
<table id="table_recive" border="1" style="width:100%;">
<tr>
<th style="width:200px;">ชื่อสินค้า</th>
<th style="width:200px;">รหัสสินค้าของผู้ขาย</th>
<th style="width:200px;">จำนวน</th>
<th style="width:200px;">คลังสินค้า</th>
<th style="width:200px;">วันที่รับ</th>
<th style="width:200px;">รับสินค้า</th>
</tr>
</table>
</div>
<div style="position: absolute; left:0px; top:60.01%;width:100%; height: 40%;">
<label style="position: absolute; left:100px; top:22px">หมายเหตุ</label>
<textarea rows="4" cols="70" style="position: absolute; left:100px; top:40px"></textarea>
<label style="position: absolute; left:700px; top:22px">จำนวนที่สั่ง</label>
<label style="position: absolute; left:700px; top:40px">จำนวนที่รับ</label>
<label style="position: absolute; left:780px; top:22px">100</label>
<label style="position: absolute; left:780px; top:40px">100</label>
<input type="button" value="รับของทั้งหมด" onclick="" style="position: absolute; left:350px; top:170px;"/>
</div>
</div>

<div id="puchese_payment" style="position: absolute; left:0px; top:160px;width:90%; height: 65%;visibility: hidden;">
<div style="position: absolute;overflow:scroll; left:0px; top:0px;width:100%; height: 60%;">
<table id="table_dat1" border="1" style="width:100%;">
<tr>
<th style="width:200px;">ชื่อสินค้า</th>
<th style="width:200px;">รหัสสินค้าของผู้ขาย</th>
<th style="width:200px;">จำนวน</th>
<th style="width:200px;">ราคาต่อหน่วย</th>
<th style="width:200px;">ส่วนลด</th>
<th style="width:200px;">ราคารวม</th>
</tr>
</table>
</div>
<div style="position: absolute; left:0px; top:60.01%;width:100%; height: 40%;">
<label style="position: absolute; left:0px; top:0px">ชำระเงินโดย</label>
<select id="p_warehouse" name="p_warehouse" style="position: absolute; left:150px; top:0px;width:100px">
<option value="Cash">Cash</option>
<option value="Master Card">Master Card</option>
<option value="VISA">VISA</option>
<option value="Paypal">Paypal</option>
</select>
<label style="position: absolute; left:0px; top:40px">วันที่จ่าย</label>
<input id="p_id" name="p_id" type="text" style="position: absolute; left:150px; top:40px">
<label style="position: absolute; left:350px; top:20px">หมายเหตุ</label>
<textarea rows="4" cols="50" style="position: absolute; left:350px; top:40px"></textarea>
<label style="position: absolute; left:700px; top:20px">ราคารวม</label>
<label style="position: absolute; left:770px; top:20px">11111110</label>
<label style="position: absolute; left:700px; top:60px">จ่ายแล้ว</label>
<input id="p_id" name="p_id" type="text" style="position: absolute; left:770px; top:60px">
<label style="position: absolute; left:700px; top:100px">ค้างชำระ</label>
<label style="position: absolute; left:770px; top:100px">11111110</label>
<input type="button" value="ชำระทั้งหมด" onclick="" style="position: absolute; left:350px; top:170px;"/>
</div>
</div>

<div id="puchese_return" style="position: absolute; left:0px; top:160px;width:90%; height: 65%;visibility: hidden;">
<div style="position: absolute;overflow:scroll; left:0px; top:0px;width:100%; height: 60%;">
<table id="table_dat1" border="1" style="width:100%;">
<tr>
<th style="width:200px;">ชื่อสินค้า</th>
<th style="width:200px;">รหัสสินค้าของผู้ขาย</th>
<th style="width:200px;">จำนวน</th>
<th style="width:200px;">ราคาต่อหน่วย</th>
<th style="width:200px;">ส่วนลด</th>
<th style="width:200px;">ราคารวม</th>
</tr>
</table>
</div>
<div style="position: absolute; left:0px; top:60.01%;width:100%; height: 40%;">
<label style="position: absolute; left:0px; top:0px">วันที่ส่งคืน</label>
<input id="p_id" name="p_id" type="text" style="position: absolute; left:150px; top:0px">

<label style="position: absolute; left:350px; top:22px">หมายเหตุ</label>
<textarea rows="4" cols="50" style="position: absolute; left:350px; top:40px"></textarea>
<label style="position: absolute; left:700px; top:20px">ราคารวม</label>
<label style="position: absolute; left:770px; top:20px">11111110</label>
<label style="position: absolute; left:700px; top:60px">Fee</label>
<input id="p_id" name="p_id" type="text" style="position: absolute; left:770px; top:60px">
<label style="position: absolute; left:700px; top:100px">เงินส่งคืน</label>
<input id="p_id" name="p_id" type="text" style="position: absolute; left:770px; top:100px">
<label style="position: absolute; left:700px; top:140px">ค้างส่งคืน</label>
<label style="position: absolute; left:770px; top:140px">11111110</label>
<input type="button" value="ส่งเงินคืนเต็มจำนวน" onclick="" style="position: absolute; left:350px; top:170px;"/>
</div>
</div>


<div id="puchese_unstock" style="position: absolute; left:0px; top:160px;width:90%; height: 65%;visibility: hidden;">
<div style="position: absolute;overflow:scroll; left:0px; top:0px;width:100%; height: 60%;">
<table id="table_dat1" border="1" style="width:100%;">
<tr>
<th style="width:200px;">ชื่อสินค้า</th>
<th style="width:200px;">จำนวน</th>
<th style="width:200px;">คลังสินค้า</th>
<th style="width:200px;">ส่งคืน</th>
</tr>
</table>
</div>
<div style="position: absolute; left:0px; top:60.01%;width:100%; height: 40%;">

<label style="position: absolute; left:10px; top:22px">หมายเหตุ</label>
<textarea rows="4" cols="140" style="position: absolute; left:10px; top:40px"></textarea>

<input type="button" value="ขนสินค้าคืนทั้งหมด" onclick="" style="position: absolute; left:350px; top:170px;"/>
</div>
</div>

</div>

</body>
</html>