<%@page import="getdat.getDatabase"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="java.util.*,com.mongodb.*"%>
<%
	HttpSession ses = request.getSession();
	String usn = (String) ses.getAttribute("usn");
	String pwd = (String) ses.getAttribute("pwd");
	String user_stat = (String) ses.getAttribute("user_stat");
	String company_name=(String) ses.getAttribute("company_name");
	String company_id=(String) ses.getAttribute("company_id");
	if(user_stat.equals("")||user_stat==null){
		
		String wr="Wrong ID or Password";
		request.setAttribute("wrong", wr);
		RequestDispatcher view = request.getRequestDispatcher("start.jsp?note="+wr+"");
		view.forward(request, response);
					    
		}
	int row1 = getDatabase.getRowCount("manufacture", "Company_ID", company_id);
	DBObject[] manudat=null;
	DBObject[] matdat=null;
	manudat=getDatabase.getCollData("manufacture", "Company_ID", company_id, row1, true, "Build_Stat", -1);
	String[] menu_id=new String[row1+1];
	String[] menu_name=new String[row1+1];
	String[] menu_amount=new String[row1+1];
	String[] menu_stat=new String[row1+1];
	String[] menu_pstat=new String[row1+1];
	String[] menu_bstat=new String[row1+1];
	String[] menu_ware=new String[row1+1];
	for(int i=1;i<=row1;i++){
		menu_id[i]=(String)manudat[i].get("Manufacture_ID");
		menu_name[i]=(String)manudat[i].get("Manufacture_Name");
		menu_amount[i]=(String)manudat[i].get("Manufacture_Amount");
		menu_stat[i]=(String)manudat[i].get("Build_Stat");
		menu_pstat[i]=(String)manudat[i].get("Part_Stat");
		menu_bstat[i]=(String)manudat[i].get("Buy_Approve");
		menu_ware[i]=(String)manudat[i].get("Warehouse_ID");
	}
	String root_url=(String)ses.getAttribute("root_url");
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript" charset="UTF-8" src="./js/util.js"></script>
<script type="text/javascript" src="./js/JSPDF/jspdf.js"></script>
<script type="text/javascript" src="./js/JSPDF/jspdf.plugin.standard_fonts_metrics.js"></script>
	<script type="text/javascript" src="./js/JSPDF/jspdf.plugin.split_text_to_size.js"></script>
	<script type="text/javascript" src="./js/JSPDF/jspdf.plugin.from_html.js"></script>

</head>
<body>
<div style="position: absolute; left:0%; top:0%;width:100%; height:350px ;overflow:scroll;">
<table id="table_dat1" border="1" style="position: absolute; left:0%; top:0%;">
  <tr>
    <th>รหัสการผลิตสินค้า</th>
    <th>ชื่อสินค้า</th>
    <th>จำนวนผลิต</th>
    <th>เปิดใบสั่งซื้อ</th>
    <th>อนุมัติการสั่งซื้อ</th>
    <th>สถานะการผลิต</th>
    <th>ดูรายการสั่งซื้อ</th>
    <th>คลังสินค้า</th>
    <th>สถานะการรับของทั้งหมด</th>      
  </tr>  
  <% 
  for(int i=1;i<=row1;i++){
	  out.println("<tr>");
	  out.println("<td>"+menu_id[i]+"</td>");
	  out.println("<td>"+menu_name[i]+"</td>");
	  out.println("<td>"+menu_amount[i]+"</td>");
	  out.println("<td><input type=\"button\" value=\"พิมพ์ใบขอซื้อ\" onclick=\"\"/></td>");	  
	  if(menu_bstat[i]!=null){
		  	if(menu_bstat[i].equalsIgnoreCase("0")){
			 	 out.println("<td><input id=\"buy_"+menu_id[i]+"\" type=\"button\" value=\"อนุมัติสั่งซื้อวัตถุดิบ\" onclick=\"approve_buy_manufacture('"+company_id+"','"+menu_id[i]+"')\"/></td>");
		  	}else{
				  out.println("<td><input id=\"buy_"+menu_id[i]+"\" type=\"button\" value=\"อนุมัติแล้ว\" onclick=\"\"/></td>");
		 	 }
		  }else{
			  out.println("<td><input id=\"buy_"+menu_id[i]+"\" type=\"button\" value=\"อนุมัติสั่งซื้อวัตถุดิบ\" onclick=\"approve_buy_manufacture('"+company_id+"','"+menu_id[i]+"')\"/></td>");
		  }
	  if(menu_stat[i]!=null){
	  	if(menu_stat[i].equalsIgnoreCase("0")){
		 	 out.println("<td>การผลิตยังไม่เสร็จสิ้น</td>");
	  	}else{
			  out.println("<td>การผลิตเสร็จสิ้นตามขั้นตอน</td>");
	 	 }
	  }else{
		  out.println("<td>การผลิตยังไม่เสร็จสิ้น</td>");
	  }
	  out.println("<td><input type=\"button\" value=\"ดูรายการสั่งซื้อ\" onclick=\"show_material('"+menu_id[i]+"','"+company_id+"',"+menu_amount[i]+")\"/></td>");
	  out.println("<td>"+menu_ware[i]+"</td>");
	  if(menu_pstat[i]!=null){
		  	if(menu_pstat[i].equalsIgnoreCase("0")){
			 	 out.println("<td>ส่งของยังไม่ครบ</td>");
		  	}else{
				  out.println("<td>ส่งของครบ</td>");
		 	 }
		  }else{
			  out.println("<td>ส่งของยังไม่ครบ</td>");
		  }	  
	  out.println("<td><input type=\"button\" value=\"Delete\" onclick=\"window.open('"+root_url+"E_edit?dv="+menu_id[i]+"&dd=yes&task=del_mat','_self')\"/></td>");
	  out.println("</tr>");
  }
 
  %>
   
</table>
</div>
<div style="position: absolute;overflow:scroll; left:0%; top:380px;width:100%; height: 350px;">
<table id="table_dat2" border="1" style="position: absolute; left:0%; top:0%;width:130%;">
  <tr>
    <th>หมายเลขส่วนประกอบ</th>
    <th>วัสดุ</th>
    <th>รายละเอียดวัสดุ</th>
    <th>จำนวนต่อหนึ่งหน่วยผลิต</th>
    <th>น้ำหนักต่อชิ้น</th>
    <th>ปริมาตรต่อชิ้น</th>
    <th>ราคาต่อชิ้น</th>
    <th>จำนวนรวม</th>
    <th>น้ำหนักรวม</th>
    <th>ปริมาตรรวม</th>
    <th>ราคารวม</th>
    <th>ผู้ขาย</th>
  </tr>  
  </table>
</div>
</body>
</html>