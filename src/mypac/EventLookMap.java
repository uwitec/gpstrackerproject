package mypac;
import java.io.*;
import java.util.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.DBObject;

public class EventLookMap extends HttpServlet {
	static String user_stat="company";
	static String usn="";
	static String pwd="";
	
	public static void doWork(HttpServletRequest req, HttpServletResponse resp,boolean isPost)
			throws ServletException, IOException {
		HttpSession ses = req.getSession();
		DBObject[] eventdat =(DBObject[])ses.getAttribute("eventdat");
		int tar = Integer.parseInt((String)req.getParameter("tar"));
		String lat =(String) eventdat[tar].get("lat");
		String lon =(String) eventdat[tar].get("lon"); 
		// web code html
			resp.setContentType("text/html");
			PrintWriter out =resp.getWriter();
		
			out.println(" <!DOCTYPE html>"); 
			out.println("<html>"); 
			out.println("<head>");
			out.println("<script type=\"text/javascript\"");
			out.println("src=\"./js/infowin.js\">");
			out.println("</script>");
			out.println("<script type=\"text/javascript\"");
			out.println("src=\"./njs/Gmap.js\">");
			out.println("</script>");
			out.println("<script type=\"text/javascript\"");
			out.println("src=\"http://maps.googleapis.com/maps/api/js?key=AIzaSyAT47leMWFTUMX6SiYeGfGMDJoZqa4BwZM&sensor=false\">");
			out.println("</script>");
		
			out.println("<script type=\"text/javascript\">");
			out.println("function initialize_GMAP() {");
			out.println("var myOptions = {");
			out.println("center: new google.maps.LatLng("+lat+", "+lon+"),");
			
			out.println("zoom: 10,");
			out.println("mapTypeId: google.maps.MapTypeId.ROADMAP");
			out.println("};");
			out.println("var map = new google.maps.Map(document.getElementById(\"map_canvas\"),myOptions);");
			out.println("var infowindow = new google.maps.InfoWindow();");
		if(1>0){
			for(int i=1;i<=1;i++){
					out.println("var point"+i+" ={");
					out.println("position: new google.maps.LatLng("+lat+", "+lon+"),");    
					out.println("title:\"Hello World! "+lat+"  "+lon+" \"");
					out.println("};");
					out.println("var marker"+i+" = new google.maps.Marker(point"+i+");");
					out.println("marker"+i+".setMap(map);");
					
					//	out.println("}");
					out.println("google.maps.event.addListener(marker"+i+", 'click', function() {");
					out.println("get_infoMarker(map,marker"+i+",infowindow);");
					out.println("})");
			}//end for make marker
		}
			out.println("}");
			out.println("</script>");
			out.println("<script type=\"text/javascript\">");
			out.println("function updateCarID()");
			out.println("{");
			out.println("var carID= document.getElementById('vehicle_ID').value;");
			out.println("document.getElementById(\"carID\").innerHTML=carID;");
			out.println("}");
			out.println("</script>");
			out.println("</head>");

			out.println("<body onload=\"initialize_GMAP()\">");
			
			out.println("<div id=\"map_canvas\" style=\"height:600px;width:800px;float:left;\">");
			out.println("</div>");
			
			out.println("</body>");
			
			out.println("</html>");
			System.gc();
		
		
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doWork(req, resp,false);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doWork(req, resp,true);
	}

}