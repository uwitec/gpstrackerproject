package adddata;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AddDevice2 extends HttpServlet {
	static String user_stat="";
	static String usn="";
	static String pwd="";
	static String company_id="";
	static String role="";
	static String name="";
	static String imei="";
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp,false);
	}	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp,true);
	}
	public void doWork(HttpServletRequest req, HttpServletResponse resp,boolean isPost)
			throws ServletException, IOException {
		HttpSession ses = req.getSession();
		usn=(String) ses.getAttribute("usn");
		pwd=(String) ses.getAttribute("pwd");
		user_stat=(String) ses.getAttribute("user_stat");
		company_id=(String) ses.getAttribute("company_id");
		imei=req.getParameter("imei");
		boolean dup = getdat.getDatabase.checkDupdata("device", "imei", imei);
		
		resp.setContentType("text/html");
		PrintWriter out =resp.getWriter();
		
			
			out.println("<html>");
			out.println("<head>");
			out.println("<script type=\"text/javascript\">");
			out.println("</script>");
			out.println("</head>");
			out.println("<body>");
			if(dup){
				out.println("imei already exit pls try anoter");	
			}else if(imei.length()!=15){
				out.println("IMEI length incorrect");
			}else if(imei.equals("")){
				out.println("IMEI field is empty");
			}else if(imei.equals("null")){
				out.println("IMEI field is empty");
			}else{
				if(company_id==null){
					out.println("Session out Add device faild!!");
				}else{
					boolean ch=insDB.PutDB.ins_device(imei, company_id);
					if(ch){
						out.println("Add Device Success!!");
					}else{
						out.println("Add device faild!!");
					}
				}
			}
			out.println("</body>");
			out.println("</html>");
		
	}

}
