package adddata;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AddDevice extends HttpServlet {
	static String user_stat="";
	static String usn="";
	static String pwd="";
	static String company_id="";
	static String role="";
	static String name="";
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp,false);
	}	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp,true);
	}
	public void doWork(HttpServletRequest req, HttpServletResponse resp,boolean isPost)
			throws ServletException, IOException {
		HttpSession ses = req.getSession();
		usn=(String) ses.getAttribute("usn");
		pwd=(String) ses.getAttribute("pwd");
		user_stat=(String) ses.getAttribute("user_stat");
		company_id=(String) ses.getAttribute("company_id");
		
		resp.setContentType("text/html");
		PrintWriter out =resp.getWriter();
		
			
			out.println("<html>");
			out.println("<head>");
			out.println("<script type=\"text/javascript\">");

			out.println("</script>");
			out.println("</head>");
			out.println("<body onload =\"main()\">");
			out.println("<h1 style=\"margin-bottom:0;\" align=\"center\">Add Device <br /><br /></h1>");	
			
			out.println("<form name=\"adddevice\" method=\"post\" action=\"adddevice2\">");
			out.println("Device Imei...............: <input type=\"text\" name=\"imei\" /><br />");
			out.println("<br />");
			out.println("<input type=\"submit\" value=\"Add Device\" />");
			out.println("</form>");
			out.println("</body>");
			out.println("</html>");
		
	}

}
