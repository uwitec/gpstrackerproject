package adddata;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.DBObject;

public class AddDeliver extends HttpServlet {
	static String user_stat="";
	static String usn="";
	static String pwd="";
	static String company_id="";
	static String role="";
	static String name="";
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp,false);
	}	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp,true);
	}
	public void doWork(HttpServletRequest req, HttpServletResponse resp,boolean isPost)
			throws ServletException, IOException {
		DBObject[] vehicleData = null;
		DBObject[] driverData = null;
		HttpSession ses = req.getSession();
		usn=(String) ses.getAttribute("usn");
		pwd=(String) ses.getAttribute("pwd");
		user_stat=(String) ses.getAttribute("user_stat");
		company_id=(String) ses.getAttribute("company_id");
		int rowcount=getdat.getDatabase.getVehicleCompanyRow(company_id);
		vehicleData=getdat.getDatabase.getVehicleCompany(company_id, rowcount);
		String[] vehicle_id= new String[rowcount+1];
		if(vehicleData!=null){
			for(int i=1;i<=rowcount;i++){
				vehicle_id[i]=""+(String) vehicleData[i].get("stat");
				if(vehicle_id[i].equals("0")){
					vehicle_id[i]=(String) vehicleData[i].get("Vehicle_ID");
				}else{
					vehicle_id[i]=null;
				}
			}
		}else{
			System.out.println("no vehicle Data");
		}
		int rowcount2=getdat.getDatabase.getRowCount("driver", "Company_ID", company_id);
		driverData=getdat.getDatabase.getDriverCompany(company_id, rowcount2);
		String[] driver_id= new String[rowcount2+1];
		if(driverData!=null){
			for(int i=1;i<=rowcount2;i++){
				driver_id[i]=""+(String) driverData[i].get("stat");
				if(driver_id[i].equals("0")){
					driver_id[i]=(String) driverData[i].get("Driver_ID");
				}else{
					driver_id[i]=null;
				}
			}
		}else{
			System.out.println("no driver Data");
		}
		resp.setContentType("text/html");
		PrintWriter out =resp.getWriter();
		
			
			out.println("<html>");
			out.println("<head>");
			out.println("<script type=\"text/javascript\">");

			out.println("</script>");
			out.println("</head>");
			out.println("<body onload =\"main()\">");
			out.println("<h1 style=\"margin-bottom:0;\" align=\"center\">Add Deliver <br /><br /></h1>");	
			
		
			out.println("<form name=\"adddevice\" method=\"post\" action=\"adddeliver2\">");
			out.println("Vehicle ID.....................:<select id=\"vehicle_id\" name=\"vehicle_id\">");
			
			for(int i=1;i<=rowcount;i++){
				if(vehicle_id[i]==null){
					
				}else{
					out.println("<option value=\""+vehicle_id[i]+"\">"+vehicle_id[i]+"</option>");
					
				}
			}
			out.println("</select>");
			out.println("<br />");
			
			out.println("Driver ID.....................:<select id=\"driver_id\" name=\"driver_id\">");
			for(int i=1;i<=rowcount2;i++){
				if(driver_id[i]==null){
					System.out.println("������ػ�ó�");
				}else{
					out.println("<option value=\""+driver_id[i]+"\">"+driver_id[i]+"</option>");
				}
			}
			out.println("</select>");
			out.println("<br />");
			
			out.println("Destination...............: <input type=\"text\" name=\"destination\" /><br />");
			out.println("Origin...............: <input type=\"text\" name=\"origin\" /><br />");
			out.println("<input type=\"submit\" value=\"Add deliver\" />");
			out.println("</form>");
			out.println("</body>");
			out.println("</html>");
		
	}

}