package adddata;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AddDriver2 extends HttpServlet {
	static String user_stat="";
	static String usn="";
	static String pwd="";
	static String company_id="";
	static String role="";
	static String driver_name_first="";
	static String driver_name_last="";
	static String province="";
	static String city="";
	static String address="";
	static String phone="";
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp,false);
	}	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp,true);
	}
	public void doWork(HttpServletRequest req, HttpServletResponse resp,boolean isPost)
			throws ServletException, IOException {
		HttpSession ses = req.getSession();
		usn=(String) ses.getAttribute("usn");
		pwd=(String) ses.getAttribute("pwd");
		user_stat=(String) ses.getAttribute("user_stat");
		company_id=(String) ses.getAttribute("company_id");
		driver_name_first=req.getParameter("first_name");
		driver_name_last=req.getParameter("last_name");
		province=req.getParameter("province");
		String year=req.getParameter("year");
		String month=req.getParameter("month");
		String day=req.getParameter("day");
		String sex=req.getParameter("sex");
		city=req.getParameter("city");
		address=req.getParameter("address");
		String email=req.getParameter("email");
		phone=req.getParameter("phone");
		boolean dupfirst = getdat.getDatabase.checkDupdata("driver", "First_name", driver_name_first);
		boolean duplast = getdat.getDatabase.checkDupdata("driver", "Last_name", driver_name_last);
		resp.setContentType("text/html");
		PrintWriter out =resp.getWriter();
		
			
			out.println("<html>");
			out.println("<head>");
			out.println("<script type=\"text/javascript\">");
			out.println("</script>");
			out.println("</head>");
			out.println("<body>");
			if(driver_name_first.equalsIgnoreCase("")){
				out.println("first name is empty");
			}else if(province.equals("")){
				out.println("province is empty");
			}else if(city.equals("")){
				out.println("city is empty");
			}else if(address.equals("")){
				out.println("adddress is empty");
			}else if(phone.equals("")){
				out.println("phone is empty");
			}else if(driver_name_last.equals("")){
				out.println("last name is empty");
			}else if(dupfirst&&duplast){
				out.println("this driver name is already exited");
			}else{
				if(company_id==null){
					out.println("Session out can't add driver");
				}else{
					boolean flag =insDB.PutDB.ins_driver(company_id, driver_name_first, driver_name_last, province, city, address, phone, user_stat,year,month,day,sex,email);
					if(flag){
						out.println("add driver success");
					}else{
						out.println("add driver failed");
					}
				}
			}
			
			out.println("</body>");
			out.println("</html>");
		
	}

}
