package adddata;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AddVehicle extends HttpServlet {
	static String user_stat="";
	static String usn="";
	static String pwd="";
	static String company_id="";
	static String role="";
	static String name="";
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp,false);
	}	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp,true);
	}
	public void doWork(HttpServletRequest req, HttpServletResponse resp,boolean isPost)
			throws ServletException, IOException {
		HttpSession ses = req.getSession();
		usn=(String) ses.getAttribute("usn");
		pwd=(String) ses.getAttribute("pwd");
		user_stat=(String) ses.getAttribute("user_stat");
		company_id=(String) ses.getAttribute("company_id");
		int rowcount = getdat.getDatabase.getRowCount("device", "Company_ID", company_id);
		String[] device_id = new String[rowcount+1];
		boolean flag = getdat.getDatabase.getDevice_company(usn, pwd, device_id, company_id,"1");
		resp.setContentType("text/html");
		PrintWriter out =resp.getWriter();
		
			
			out.println("<html>");
			out.println("<head>");
			out.println("<script type=\"text/javascript\">");

			out.println("</script>");
			out.println("</head>");
			out.println("<body onload =\"main()\">");
			out.println("<h1 style=\"margin-bottom:0;\" align=\"center\">Add Vehicle <br /><br /></h1>");	
			if(!flag){
			out.println("no device ID found please add device first <br />");
			}
			out.println("<form name=\"addvehicle\" method=\"post\" action=\"addvehicle2\">");
			out.println("Car register ID...............: <input type=\"text\" name=\"car_register_id\" /><br />");
			out.println("Type..........................: <input type=\"text\" name=\"type\" /><br />");
			out.println("Device ID.....................:<select id=\"device_id\" name=\"device_id\">");
			for(int i=1;i<=rowcount;i++){
				if(device_id[i]==null){
					
				}else{
					out.println("<option value=\""+device_id[i]+"\">"+device_id[i]+"</option>");
				}
			}
			out.println("</select>");
			out.println("<br />");
			out.println("<input type=\"submit\" value=\"Add Vehicle\" />");
			out.println("</form>");
			out.println("</body>");
			out.println("</html>");
		
	}

}
