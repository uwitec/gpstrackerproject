package adddata;
import insDB.PutDB;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.mongodb.DBObject;
public class AddDeliver2 extends HttpServlet {
	static String user_stat="";
	static String usn="";
	static String pwd="";
	static String company_id="";
	static String role="";
	static String name="";
	static String driver_id="";
	static String vehicle_id="";
	static String destination="";
	static String origin="";
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp,false);
	}	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp,true);
	}
	public void doWork(HttpServletRequest req, HttpServletResponse resp,boolean isPost)
			throws ServletException, IOException {
		HttpSession ses = req.getSession();
		usn=(String) ses.getAttribute("usn");
		pwd=(String) ses.getAttribute("pwd");
		user_stat=(String) ses.getAttribute("user_stat");
		company_id=(String) ses.getAttribute("company_id");
		vehicle_id=""+req.getParameter("vehicle_id");
		driver_id=""+req.getParameter("driver_id");
		destination=req.getParameter("destination");
		origin=req.getParameter("origin");
		int deliver_p_row =Integer.parseInt(req.getParameter("prow"));
		resp.setContentType("text/html");
		PrintWriter out =resp.getWriter();
		
			
			out.println("<html>");
			out.println("<head>");
			out.println("<script type=\"text/javascript\">");
			
			out.println("</script>");
			out.println("</head>");
			out.println("<body onload =\"main()\">");
			if(vehicle_id.equalsIgnoreCase("null")){
				out.println("vehicle ID is empty pls add");
			}else if(driver_id.equalsIgnoreCase("null")){
				out.println("driver_ID is empty pls add");
			}else if(destination.equals("")){
				out.println("no destination");
			}else if(origin.equalsIgnoreCase("")){
				out.println("no origin");
			}
			else{				
				String ch = PutDB.ins_deliver(vehicle_id, driver_id, company_id, destination,origin);
				if(!ch.equalsIgnoreCase("")){
					out.println("success!!!!");
				}else{
					out.println("Add Deliver Failed!!");
				}
				boolean ch_p=false;
				for(int i=1;i<=deliver_p_row;i++){
					out.println(i+ "  **********************<br />Product ID :"+req.getParameter("product_id_"+i)+"||"+"Product Name :"+req.getParameter("product_name_"+i)+"||"+"Price :"+req.getParameter("price_per_unit_"+i)+"||"+"Amount :"+req.getParameter("amount_"+i)+"<br />"+"Customer ID :"+req.getParameter("customer_"+i)+"<br />");
					ch_p=insDB.PutDB.ins_product(company_id, ch, req.getParameter("product_id_"+i), req.getParameter("product_name_"+i), req.getParameter("description_"+i), req.getParameter("price_per_unit_"+i), req.getParameter("amount_"+i), req.getParameter("customer_"+i));
					if(!ch_p){
						out.println("Add product ID :"+req.getParameter("product_id_"+i)+"  FAILED!!!");
					}
				}
			}
			
			out.println("</body>");
			out.println("</html>");
		
	}

}
