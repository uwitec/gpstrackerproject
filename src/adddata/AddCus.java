package adddata;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AddCus extends HttpServlet {
	static String user_stat="";
	static String usn="";
	static String pwd="";
	static String company_id="";
	static String role="";
	static String name="";
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp,false);
	}	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp,true);
	}
	public void doWork(HttpServletRequest req, HttpServletResponse resp,boolean isPost)
			throws ServletException, IOException {
		HttpSession ses = req.getSession();
		usn=(String) ses.getAttribute("usn");
		pwd=(String) ses.getAttribute("pwd");
		user_stat=(String) ses.getAttribute("user_stat");
		company_id=(String) ses.getAttribute("company_id");
		String q1_customerName =req.getParameter("q1_customerName");
		String q4_country =req.getParameter("q4_country");
		String q3_province =req.getParameter("q3_province");
		String q5_city =req.getParameter("q5_city");
		String q7_address =req.getParameter("q7_address");
		String q6_phone =req.getParameter("q6_phone");
		String year =req.getParameter("birthday_year");
		String month =req.getParameter("birthday_month");
		String day =req.getParameter("birthday_day");
		String sex =req.getParameter("sex");
		String email =req.getParameter("email");
		boolean dupfirst = getdat.getDatabase.checkDupdata("customer", "Customer_name", q1_customerName);
		
		resp.setContentType("text/html");
		PrintWriter out =resp.getWriter();
		
			
			out.println("<html>");
			out.println("<head>");
			out.println("<script type=\"text/javascript\">");
			out.println("</script>");
			out.println("</head>");
			out.println("<body>");
			 if(dupfirst){
				out.println("this customer name is already exited");
			}else if(company_id==null){
				out.println("Sesion out add driver failed ");
			}
			 else{
				boolean flag =insDB.PutDB.ins_Customer(company_id, q1_customerName, q4_country, q3_province, q5_city, q7_address, q6_phone,year,month,day,sex,email);
				if(flag){
					out.println("add driver success");
				}else{
					out.println("add driver failed");
				}
			}
			
			out.println("</body>");
			out.println("</html>");
		
	}
	
	
}