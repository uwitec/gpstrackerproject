package adddata;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AddVehicle2 extends HttpServlet {
		static String user_stat="";
		static String usn="";
		static String pwd="";
		static String company_id="";
		static String registerID="";
		static String type="";
		static String vehicle_id="";
		static String device_id="";
		public void doGet(HttpServletRequest req, HttpServletResponse resp)
				throws ServletException, IOException {
			
			doWork(req, resp,false);
		}	
		public void doPost(HttpServletRequest req, HttpServletResponse resp)
				throws ServletException, IOException {
			
			doWork(req, resp,true);
		}
		public void doWork(HttpServletRequest req, HttpServletResponse resp,boolean isPost)
				throws ServletException, IOException {
			HttpSession ses = req.getSession();
			usn=(String) ses.getAttribute("usn");
			pwd=(String) ses.getAttribute("pwd");
			user_stat=(String) ses.getAttribute("user_stat");
			company_id=(String) ses.getAttribute("company_id");
			registerID= req.getParameter("car_register_id");
			device_id =""+req.getParameter("device_id");
			type =req.getParameter("type");
			String year =req.getParameter("year");
			String month =req.getParameter("month");
			String day =req.getParameter("day");
			String gear =req.getParameter("gear");
			String brand =req.getParameter("brand");
			String model =req.getParameter("model");
			String color =req.getParameter("color");
			String description =req.getParameter("description");
			String engine =req.getParameter("engine");
			boolean ch =getdat.getDatabase.checkDupdata("vehicle","RegisterID",registerID);
			resp.setContentType("text/html");
			PrintWriter out =resp.getWriter();
			
			out.println(" <!DOCTYPE html>"); 
			out.println("<html>"); 
			out.println("<head>");
			
			out.println("</head>");

			out.println("<body >");
			if(device_id.equals("null")){
				out.println("device ID empty please add device <br />");
			}else if(registerID.equals("")){
				out.println("register ID empty new add <br />");
				
			}else if(ch){
				out.println("register ID duplicate <br />");
			}
			else{
				boolean flag = false;
				flag = insDB.PutDB.ins_vehicle(device_id, registerID, company_id, type, user_stat,year,month,day,gear,brand,model,color,description,engine);
				if(flag){
					out.println("success!!!");
				}else{
					out.println("ADD VEHICLE FAILED!!");
				}
			}
			out.println("</body>");
			
			out.println("</html>");
			
			
		}

}
