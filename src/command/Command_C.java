package command;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mypac.PrintMenuCompany;

public class Command_C extends HttpServlet {
	static String usn = "";
	static String pwd = "";
	static String user_stat = "";
	private static String com_code="";	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		doWork(req, resp, false);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		doWork(req, resp, true);
	}

	public static void doWork(HttpServletRequest req, HttpServletResponse resp,
			boolean isPost) throws ServletException, IOException {
		HttpSession ses = req.getSession();
		usn = (String) ses.getAttribute("usn");
		pwd = (String) ses.getAttribute("pwd");
		com_code=req.getParameter("code");
		user_stat = getdat.getDatabase.getUserdata(usn, pwd);
		resp.setContentType("text/html");
		PrintWriter out =resp.getWriter();

		if (user_stat.equals("null")) {
			// System.out.println("work in null case");
			String wr = "Wrong ID or Password";
			req.setAttribute("wrong", wr);
			RequestDispatcher view = req.getRequestDispatcher("../start.jsp?note="+ wr + "");
			view.forward(req, resp);

		} else if (user_stat.equals("admin")) {

		} else if (user_stat.equals("customer")) {

		} else if (user_stat.equals("company")) {
			try {
			Socket skt = new Socket();
	         skt.connect(new InetSocketAddress("161.246.22.68",6100),10000);
	         System.out.print("Sending string: ' ");	         
	         PrintWriter outstream = new PrintWriter(skt.getOutputStream(), true);	         
	         outstream.println(com_code);
	         skt.close();
	         out.print("<script type=\"text/javascript\">document.location.href=\"./command_veh.jsp\";</script>");
			}catch(Exception e) {
		         out.print("something wrong when connect and send");
		         //e.printStackTrace();
		      }
	         
		} else {
			String wr = "Wrong ID or Password";
			req.setAttribute("wrong", wr);
			RequestDispatcher view = req.getRequestDispatcher("../start.jsp?p1=123456");
			view.forward(req, resp);
		}
		System.gc();
	}

}
