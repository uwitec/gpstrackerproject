package editRead;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.*;

public class R_report_page extends HttpServlet {
	static String usn="";
	static String pwd ="";
	static String user_stat="";
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp, false);
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp, true);
	}
	public static void doWork(HttpServletRequest req, HttpServletResponse resp,boolean isPost)throws ServletException, IOException {
		
		HttpSession ses = req.getSession();
		ses.setAttribute("wr1", "Session out");
		int page1=Integer.parseInt(req.getParameter("pg"));
		usn =(String) ses.getAttribute("usn");
		pwd =(String) ses.getAttribute("pwd");
		user_stat=getdat.getDatabase.getUserdata(usn, pwd);
		String company_id = (String) ses.getAttribute("company_id");
		String vehicle_id =(String) ses.getAttribute("Vehicle_ID");
		DBObject vehicleData = getdat.getDatabase.getCollData("vehicle", "Vehicle_ID", vehicle_id);
		String deliver_id = getdat.getDatabase.getActiveDeliverID(vehicle_id);
		int rowcount=getdat.getDatabase.getRowCount("eventdata", "Vehicle_ID", vehicle_id);
		DBObject[] eventData=null;
		resp.setContentType("text/html");
		PrintWriter out =resp.getWriter();
		if(user_stat.equals("")){
			
			String wr="Wrong ID or Password";
			req.setAttribute("wrong", wr);
			RequestDispatcher view = req.getRequestDispatcher("start.jsp?p1=123456");
			view.forward(req, resp);
						    
		}
		out.println(" <!DOCTYPE html>"); 
		out.println("<html>"); 
		out.println("<head>");
		
		out.println("</head>");

		out.println("<body >");
		if(rowcount>0){
			
			eventData=getdat.getDatabase.geteventdata(vehicle_id,rowcount);
			ses.setAttribute("eventdat", eventData);
			ses.setAttribute("row", rowcount);
			ses.setAttribute("pg", page1);
			RequestDispatcher view = req.getRequestDispatcher("showreport.jsp");
			view.forward(req, resp);
			
		}else{
			out.println("No data For show");
		}
		

		out.println("<body >");
		
		out.println("</body>");
		
		out.println("</html>");
		
	}
	

}