package editRead;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.DBObject;

public class R_vehicle extends HttpServlet {
	static String usn="";
	static String pwd ="";
	static String user_stat="";
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp, false);
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp, true);
	}
	public static void doWork(HttpServletRequest req, HttpServletResponse resp,boolean isPost)throws ServletException, IOException {
		
		HttpSession ses = req.getSession();
		ses.setAttribute("wr1", "Session out");
		usn =(String) ses.getAttribute("usn");
		pwd =(String) ses.getAttribute("pwd");
		user_stat=getdat.getDatabase.getUserdata(usn, pwd);
		String company_id = (String) ses.getAttribute("company_id");
		ses.setAttribute("task", "vehicle");
		if(user_stat.equals("")){
			
			String wr="Wrong ID or Password";
			req.setAttribute("wrong", wr);
			RequestDispatcher view = req.getRequestDispatcher("start.jsp?p1=123456");
			view.forward(req, resp);
						    
		}
		//System.out.println(usn+"\n"+pwd+"\n"+user_stat);
		RequestDispatcher view = req.getRequestDispatcher("readPage.jsp");
		view.forward(req, resp);
		int row = 0;
		row = getdat.getDatabase.getRowCount("vehicle", "Company_ID", company_id);
		DBObject[] deviceDat =null;
		deviceDat=getdat.getDatabase.getCollData("vehicle", "Company_ID", company_id, row);
		String[] device_id =new String[row+1];
		String[] vehicle_id =new String[row+1];
		String[] registerID=new String[row+1];
		String[] year=new String[row+1];
		String[] month=new String[row+1];
		String[] day=new String[row+1];
		String[] gear=new String[row+1];
		String[] brand=new String[row+1];
		String[] model=new String[row+1];
		String[] color=new String[row+1];
		String[] description=new String[row+1];
		String[] engine=new String[row+1];
		String[] stat=new String[row+1];
		String[] type=new String[row+1];
		for(int i=1;i<=row;i++){
			device_id[i]=(String) deviceDat[i].get("Device_ID");
			vehicle_id[i]=(String) deviceDat[i].get("Vehicle_ID");
			registerID[i]=(String) deviceDat[i].get("RegisterID");
			stat[i]=(String) deviceDat[i].get("stat");
			year[i]=(String) deviceDat[i].get("Year");
			month[i]=(String) deviceDat[i].get("Month");
			day[i]=(String) deviceDat[i].get("Day");
			gear[i]=(String) deviceDat[i].get("Gear");
			brand[i]=(String) deviceDat[i].get("Brand");
			model[i]=(String) deviceDat[i].get("Model");
			color[i]=(String) deviceDat[i].get("Color");
			description[i]=(String) deviceDat[i].get("Description");
			engine[i]=(String) deviceDat[i].get("Engine");
			stat[i]=(String) deviceDat[i].get("stat");
			type[i]=(String) deviceDat[i].get("Type");
		}
		resp.setContentType("text/html");
		PrintWriter out =resp.getWriter();
		// web code
		out.println(" <!DOCTYPE html>"); 
		out.println("<html>"); 
		out.println("<head>");
		
		out.println("</head>");

		out.println("<body >");
		
		out.println("<table border=\"1\">");
		out.println("<caption>Vehicle Detail</caption>");
		out.println("<tr><th>Device ID</th><th>Vehicle ID</th><th>Register ID</th><th>Year of purchase</th><th>Gear</th><th>Brand</th><th>Model</th><th>Color</th><th>Description</th><th>Engine</th><th>stat</th><th>Type</th></tr>");
		for(int i=1;i<=row;i++){
			out.println("<tr><td>"+device_id[i]+"</td><td>"+vehicle_id[i]+"</td><td>"+registerID[i]+ "</td><td>"+year[i]+"/"+month[i]+"/"+day[i]+"</td><td>"+gear[i]+"</td><td>"+brand[i]+"</td><td>"+model[i]+"</td><td>"+color[i]+"</td><td>"+description[i]+"</td><td>"+engine[i]+"</td><td>"+stat[i]+"</td><td>"+type[i]+"</td>");
			out.println("<td>");
			out.println("<form name=\"formmy"+i+"\"action=\"E_vehicle\" method=\"post\">");
			//out.println("<input type=\"checkbox\" id=\"dv\" checked=\"checked\" name=\"dv\" value=\"testttttt\" /> ");
			out.println("<input type=\"button\" value=\"Edit\" onclick=\"window.location.href='./E_edit?dv="+vehicle_id[i]+"&dd=0&task=vehicle'\"/>");
			out.println("<input type=\"button\" value=\"Delete\" onclick=\"window.location.href='./E_edit?dv="+vehicle_id[i]+"&dd=yes&task=vehicle'\"/>");
			out.println("</form>");
			out.println("</td>");
			out.println("</tr>");
		}
		out.println("</table>");
		out.println("</body>");
		
		out.println("</html>");
		
	}
	

}