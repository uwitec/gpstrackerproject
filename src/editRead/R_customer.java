package editRead;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.DBObject;

public class R_customer extends HttpServlet {
	static String usn="";
	static String pwd ="";
	static String user_stat="";
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp, false);
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp, true);
	}
	public static void doWork(HttpServletRequest req, HttpServletResponse resp,boolean isPost)throws ServletException, IOException {
		
		HttpSession ses = req.getSession();
		ses.setAttribute("wr1", "Session out");
		usn =(String) ses.getAttribute("usn");
		pwd =(String) ses.getAttribute("pwd");
		user_stat=getdat.getDatabase.getUserdata(usn, pwd);
		String company_id = (String) ses.getAttribute("company_id");
		ses.setAttribute("task", "customer");
		if(user_stat.equals("")){
			
			String wr="Wrong ID or Password";
			req.setAttribute("wrong", wr);
			RequestDispatcher view = req.getRequestDispatcher("start.jsp?p1=123456");
			view.forward(req, resp);
						    
		}		
		RequestDispatcher view = req.getRequestDispatcher("readPage.jsp");
		view.forward(req, resp);
		//System.out.println(usn+"\n"+pwd+"\n"+user_stat);
		int row = 0;
		row = getdat.getDatabase.getRowCount("customer", "Company_ID", company_id);
		DBObject[] deviceDat =null;
		deviceDat=getdat.getDatabase.getCollData("customer", "Company_ID", company_id, row);
		String[] customer_id =new String[row+1];
		String[] customer_name =new String[row+1];
		String[] year =new String[row+1];
		String[] month =new String[row+1];
		String[] day =new String[row+1];
		String[] sex =new String[row+1];
		String[] country=new String[row+1];
		String[] province=new String[row+1];
		String[] city=new String[row+1];
		String[] address=new String[row+1];
		String[] phone=new String[row+1];
		String[] email =new String[row+1];
		String[] stat=new String[row+1];
		for(int i=1;i<=row;i++){
			customer_id[i]=(String) deviceDat[i].get("Customer_ID");
			customer_name[i]=(String) deviceDat[i].get("Customer_name");
			country[i]=(String) deviceDat[i].get("Country");
			stat[i]=(String) deviceDat[i].get("stat");
			province[i]=(String) deviceDat[i].get("Province");
			city[i]=(String) deviceDat[i].get("City");
			address[i]=(String) deviceDat[i].get("Address");
			phone[i]=(String) deviceDat[i].get("Phone");
			year[i]=(String) deviceDat[i].get("Year");
			month[i]=(String) deviceDat[i].get("Month");
			day[i]=(String) deviceDat[i].get("Day");
			sex[i]=(String) deviceDat[i].get("Sex");
			email[i]=(String) deviceDat[i].get("E-Mail");
			
		}
		resp.setContentType("text/html");
		PrintWriter out =resp.getWriter();
		// web code
		out.println(" <!DOCTYPE html>"); 
		out.println("<html>"); 
		out.println("<head>");
		out.println("<script type=\"text/javascript\"");
		out.println("src=\"./njs/editdb.js\">");
		out.println("</script>");
		out.println("</head>");

		out.println("<body >");
		out.println("<table border=\"1\">");
		out.println("<caption>Customer Detail</caption>");
		out.println("<tr><th>Customer ID</th><th>Customer Name</th><th>Date of Birth</th><th>Sex</th><th>Country</th><th>stat</th><th>Province</th><th>City</th><th>Address</th><th>E-mail</th><th>Phone</th></tr>");
		for(int i=1;i<=row;i++){
			out.println("<tr><td>"+customer_id[i]+"</td><td>"+customer_name[i]+"</td><td>"+year[i]+"/"+month[i]+"/"+day[i]+"</td><td>"+sex[i]+"</td><td>"+country[i]+"</td><td>"+stat[i]+ "</td><td>"+province[i]+"</td><td>"+city[i]+"</td><td>"+address[i]+"</td><td> "+email[i]+"</td><td> "+phone[i]+"</td>");
			out.println("<td>");
			out.println("<form name=\"formmy"+i+"\"action=\"E_driver\" method=\"post\">");
			//out.println("<input type=\"checkbox\" id=\"dv\" checked=\"checked\" name=\"dv\" value=\"testttttt\" /> ");
			out.println("<input type=\"button\" value=\"Edit\" onclick=\"window.location.href='./E_edit?dv="+customer_id[i]+"&dd=0&task=customer'\"/>");
			out.println("<input type=\"button\" value=\"Delete\" onclick=\"window.location.href='./E_edit?dv="+customer_id[i]+"&dd=yes&task=customer'\"/>");
			out.println("</form>");
			out.println("</td>");
			out.println("</tr>");
		}
		out.println("</table>");
		
		out.println("</body>");
		
		out.println("</html>");
		
	}
	

}