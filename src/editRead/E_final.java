package editRead;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.*;

public class E_final extends HttpServlet {
	static String usn="";
	static String pwd ="";
	static String user_stat="";
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp, false);
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp, true);
	}
	public static void doWork(HttpServletRequest req, HttpServletResponse resp,boolean isPost)throws ServletException, IOException {
		
		HttpSession ses = req.getSession();
		BasicDBObject query=new BasicDBObject() ;
		usn =(String) ses.getAttribute("usn");
		pwd =(String) ses.getAttribute("pwd");
		user_stat=getdat.getDatabase.getUserdata(usn, pwd);
		DBObject dat_data=null;
		String company_id = (String) ses.getAttribute("company_id");
		ses.setAttribute("company_id", company_id);
		String task = (String)ses.getAttribute("task2");
		String dat_id = (String)ses.getAttribute("dat_id");
		if(user_stat.equals("")){
			
			String wr="Wrong ID or Password";
			req.setAttribute("wrong", wr);
			RequestDispatcher view = req.getRequestDispatcher("start.jsp?p1=123456");
			view.forward(req, resp);
						    
		}
		//System.out.println(usn+"\n"+pwd+"\n"+user_stat);
		boolean flag=false;
		resp.setContentType("text/html");
		PrintWriter out =resp.getWriter();
		// web code
		out.println(" <!DOCTYPE html>"); 
		out.println("<html>"); 
		out.println("<head>");
		
		out.println("</head>");

		out.println("<body >");
		if(task.equalsIgnoreCase("deliver")){
			dat_data =getdat.getDatabase.getCollData("deliver", "Deliver_ID", req.getParameter("deliver_id"));						
			if(!req.getParameter("driver_id").equalsIgnoreCase("")){
				query.put("Driver_ID", req.getParameter("driver_id"));
				String d_id=(String) dat_data.get("Driver_ID");
				if(!d_id.equalsIgnoreCase(req.getParameter("driver_id"))){
					BasicDBObject cc = new BasicDBObject();
					cc.put("stat", "0");
					insDB.PutDB.update_data("driver", "Driver_ID", d_id, cc);
					BasicDBObject cc2 = new BasicDBObject();
					cc2.put("stat", "1");
					insDB.PutDB.update_data("driver", "Driver_ID", req.getParameter("driver_id"), cc2);
				}
			}
			if(!req.getParameter("vehicle_id").equalsIgnoreCase("")){
				query.put("Vehicle_ID", req.getParameter("vehicle_id"));
				String d_id=(String) dat_data.get("Vehicle_ID");
				if(!d_id.equalsIgnoreCase(req.getParameter("vehicle_id"))){
					BasicDBObject cc = new BasicDBObject();
					cc.put("stat", "0");
					insDB.PutDB.update_data("vehicle", "Vehicle_ID", d_id, cc);
					BasicDBObject cc2 = new BasicDBObject();
					cc2.put("stat", "1");
					insDB.PutDB.update_data("vehicle", "Vehicle_ID", req.getParameter("vehicle_id"), cc2);
				}
			}
			query.put("stat", req.getParameter("stat"));
			query.put("Origin", new String(req.getParameter("origin").getBytes("UTF-8"), "UTF8"));
			query.put("Destination", new String(req.getParameter("destination").getBytes("UTF-8"), "UTF8"));
			flag = insDB.PutDB.update_data(task, "Deliver_ID", dat_id, query);
			out.println("<h1>Edit Success</h1>");
			/*RequestDispatcher view = req.getRequestDispatcher("rDeliver");
			view.forward(req, resp);*/
		}else if(task.equalsIgnoreCase("device")){
			//query.put("Device_ID", req.getParameter("device_id"));
			query.put("Vehicle_ID", req.getParameter("vehicle_id"));
			query.put("imei", req.getParameter("imei"));
			query.put("stat", req.getParameter("stat"));
			flag = insDB.PutDB.update_data(task, "Device_ID", dat_id, query);
			out.println("<h1>Edit Success</h1>");
			/*RequestDispatcher view = req.getRequestDispatcher("rDevice");
			view.forward(req, resp);*/
		}else if(task.equalsIgnoreCase("driver")){
			query.put("First_name", new String(req.getParameter("first_name").getBytes("UTF-8"), "UTF8"));
			query.put("Last_name", new String(req.getParameter("last_name").getBytes("UTF-8"), "UTF8"));
			query.put("stat", req.getParameter("stat"));
			query.put("Province", new String(req.getParameter("province").getBytes("UTF-8"), "UTF8"));
			query.put("City", new String(req.getParameter("city").getBytes("UTF-8"), "UTF8"));
			query.put("Address", new String(req.getParameter("address").getBytes("UTF-8"), "UTF8"));
			query.put("Phone", req.getParameter("phone"));
			flag = insDB.PutDB.update_data(task, "Driver_ID", dat_id, query);
			out.println("<h1>Edit Success</h1>");
			/*
			RequestDispatcher view = req.getRequestDispatcher("rDriver");
			view.forward(req, resp);*/
		}else if(task.equalsIgnoreCase("vehicle")){
			dat_data=getdat.getDatabase.getCollData("vehicle", "Vehicle_ID", dat_id);
			//query.put("Vehicle_ID", req.getParameter("vehicle_id"));
			query.put("RegisterID", new String(req.getParameter("registerid").getBytes("UTF-8"), "UTF8"));
			if(!req.getParameter("device_id").equalsIgnoreCase("")){
				query.put("Device_ID", req.getParameter("device_id"));
				String d_id=(String) dat_data.get("Device_ID");
				if(!d_id.equalsIgnoreCase(req.getParameter("device_id"))){
					BasicDBObject cc = new BasicDBObject();
					cc.put("stat", "0");
					cc.put("Vehicle_ID", "");
					insDB.PutDB.update_data("device", "Device_ID", d_id, cc);
					BasicDBObject cc2 = new BasicDBObject();
					cc2.put("stat", "1");
					cc2.put("Vehicle_ID", dat_id);
					insDB.PutDB.update_data("device", "Device_ID", req.getParameter("device_id"), cc2);
				}
			}
			query.put("stat", req.getParameter("stat"));
			query.put("Year", req.getParameter("year"));
			query.put("Month", req.getParameter("month"));
			query.put("Day", req.getParameter("day"));
			query.put("Gear", req.getParameter("gear"));
			query.put("Brand", req.getParameter("brand"));
			query.put("Model", req.getParameter("model"));
			query.put("Color", req.getParameter("color"));
			query.put("Description", req.getParameter("description"));
			query.put("Engine", req.getParameter("engine"));
			query.put("Type", new String(req.getParameter("type").getBytes("UTF-8"), "UTF8"));			
			flag = insDB.PutDB.update_data(task, "Vehicle_ID", dat_id, query);
			out.println("<h1>Edit Success</h1>");
			/*
			RequestDispatcher view = req.getRequestDispatcher("rVehicle");
			view.forward(req, resp);*/
			
		}
		else if(task.equalsIgnoreCase("product")){
			query.put("Product_ID", new String(req.getParameter("product_id").getBytes("UTF-8"), "UTF8"));
			query.put("Product_Name", new String(req.getParameter("product_name").getBytes("UTF-8"), "UTF8"));
			query.put("Description", new String(req.getParameter("description").getBytes("UTF-8"), "UTF8"));
			query.put("Price", req.getParameter("price"));
			query.put("Amount", req.getParameter("amount"));
			flag = insDB.PutDB.update_data(task, "Product_ID", dat_id, query);
			RequestDispatcher view = req.getRequestDispatcher("viewProduct.jsp?dv="+req.getParameter("deliver_id")+"");
			view.forward(req, resp);
			
		}		
		else if(task.equalsIgnoreCase("customer")){
			query.put("Customer_name", new String(req.getParameter("customer_name").getBytes("UTF-8"), "UTF8"));
			query.put("Year", new String(req.getParameter("year").getBytes("UTF-8"), "UTF8"));
			query.put("Month", req.getParameter("month"));
			query.put("Day", new String(req.getParameter("day").getBytes("UTF-8"), "UTF8"));
			query.put("Sex", new String(req.getParameter("sex").getBytes("UTF-8"), "UTF8"));			
			query.put("stat", req.getParameter("stat"));
			query.put("Province", new String(req.getParameter("province").getBytes("UTF-8"), "UTF8"));
			query.put("City", new String(req.getParameter("city").getBytes("UTF-8"), "UTF8"));
			query.put("Address", new String(req.getParameter("address").getBytes("UTF-8"), "UTF8"));
			query.put("E-Mail", req.getParameter("email"));
			query.put("Phone", req.getParameter("phone"));
			flag = insDB.PutDB.update_data(task, "Customer_ID", dat_id, query);
			out.println("<h1>Edit Success</h1>");
			/*RequestDispatcher view = req.getRequestDispatcher("rCustomer");
			view.forward(req, resp);*/
			
		}	
		if(flag){
			out.println("success");
		}else{
			out.println("falied");
		}
		out.println("</body>");
		
		out.println("</html>");
		
	}
	

}