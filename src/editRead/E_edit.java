package editRead;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.*;

public class E_edit extends HttpServlet {
	static String usn="";
	static String pwd ="";
	static String user_stat="";
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp, false);
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp, true);
	}
	public static void doWork(HttpServletRequest req, HttpServletResponse resp,boolean isPost)throws ServletException, IOException {
		
		HttpSession ses = req.getSession();
		ses.setAttribute("wr1", "Session out");
		usn =(String) ses.getAttribute("usn");
		pwd =(String) ses.getAttribute("pwd");
		user_stat=getdat.getDatabase.getUserdata(usn, pwd);
		String company_id = (String) ses.getAttribute("company_id");
		ses.setAttribute("company_id", company_id);
		DBObject dbdat=null;
		String dat_id= req.getParameter("dv");
		String checkDel= req.getParameter("dd");
		String name_dat= req.getParameter("task");
		String fild= req.getParameter("fild_d");
		if(user_stat.equals("")){
			
			String wr="Wrong ID or Password";
			req.setAttribute("wrong", wr);
			RequestDispatcher view = req.getRequestDispatcher("start.jsp?p1=123456");
			view.forward(req, resp);
						    
		}else{
			if(checkDel.equalsIgnoreCase("0")){
			if(name_dat.equalsIgnoreCase("deliver")){
				dbdat=getdat.getDatabase.getCollData("deliver", "Deliver_ID", dat_id);
				ses.setAttribute("dbdat", dbdat);
				ses.setAttribute("task", "deliver");
				RequestDispatcher view = req.getRequestDispatcher("editpage.jsp");
				view.forward(req, resp);
			}else if(name_dat.equalsIgnoreCase("device")){
				dbdat=getdat.getDatabase.getCollData("device", "Device_ID", dat_id);
				ses.setAttribute("dbdat", dbdat);
				ses.setAttribute("task", "device");
				RequestDispatcher view = req.getRequestDispatcher("editpage.jsp");
				view.forward(req, resp);
			}else if(name_dat.equalsIgnoreCase("driver")){
				dbdat=getdat.getDatabase.getCollData("driver", "Driver_ID", dat_id);
				ses.setAttribute("dbdat", dbdat);
				ses.setAttribute("task", "driver");
				RequestDispatcher view = req.getRequestDispatcher("editpage.jsp");
				view.forward(req, resp);
			}else if(name_dat.equalsIgnoreCase("vehicle")){
				dbdat=getdat.getDatabase.getCollData("vehicle", "Vehicle_ID", dat_id);
				ses.setAttribute("dbdat", dbdat);
				ses.setAttribute("task", "vehicle");
				RequestDispatcher view = req.getRequestDispatcher("editpage.jsp");
				view.forward(req, resp);
			}else if(name_dat.equalsIgnoreCase("product")){
				dbdat=getdat.getDatabase.getCollData("product", "Product_ID", dat_id);
				ses.setAttribute("dbdat", dbdat);
				ses.setAttribute("task", "product");
				RequestDispatcher view = req.getRequestDispatcher("editpage.jsp");
				view.forward(req, resp);
			}else if(name_dat.equalsIgnoreCase("customer")){
				dbdat=getdat.getDatabase.getCollData("customer", "Customer_ID", dat_id);
				ses.setAttribute("dbdat", dbdat);
				ses.setAttribute("task", "customer");
				RequestDispatcher view = req.getRequestDispatcher("editpage.jsp");
				view.forward(req, resp);
			}else if(name_dat.equalsIgnoreCase("group")){
				String ins =req.getParameter("ins");
				String tella="";
				if(ins.equalsIgnoreCase("1")){
					String group_name=req.getParameter("group_name");
					int rowcount =Integer.parseInt((String) ses.getAttribute("row"));
					String[] vehicle_id1 =new String[rowcount+1];
					BasicDBObject query1 = null;					
					boolean ch = getdat.getDatabase.checkDupdata("group_vehicle", "Group_Name", group_name);
					if(ch){
						tella="data dup";
					}else{
						for(int i=1;i<=rowcount;i++){
							vehicle_id1[i]= req.getParameter("VE0"+i);
							if(vehicle_id1[i]==null||vehicle_id1[i].equalsIgnoreCase("")){
							
							}else{
								query1 = new BasicDBObject();
								query1.put("Group_Name", group_name);
								query1.put("Vehicle_ID", vehicle_id1[i]);
								insDB.PutDB.insCollData("group_vehicle", query1);
							}
						}
						tella="OK";
					}
					System.gc();
				}
				resp.setContentType("text/html");
				PrintWriter out =resp.getWriter();
				// web code
				out.println(" <!DOCTYPE html>"); 
				out.println("<html>"); 
				out.println("<head>");
				out.println("<script type=\"text/javascript\"");
				out.println("src=\"./njs/editdb.js\">");
				out.println("</script>");
				out.println("</head>");
				out.println("<body >");
				out.println("test group "+tella);
				out.println("</body>");					
				out.println("</html>");
			}else{
				String wr="Wrong ID or Password";
				req.setAttribute("wrong", wr);
				RequestDispatcher view = req.getRequestDispatcher("start.jsp?p1=123456");
				view.forward(req, resp);
			}
			///////////////////////////
			}else if(checkDel.equalsIgnoreCase("yes")){
				boolean flag =false;
				boolean flag2=false;
				if(name_dat.equalsIgnoreCase("deliver")){
					DBObject deli = getdat.getDatabase.getCollData("deliver", "Deliver_ID", dat_id);
					flag = deleteDB.DelDB.remove_alldat(name_dat, "Deliver_ID", dat_id);
					BasicDBObject query =new BasicDBObject();
					BasicDBObject query2 =new BasicDBObject();
					query.put("stat", "0");
					query2.put("Deliver_ID", dat_id);
					query2.put("Company_ID", company_id);
					flag2 = insDB.PutDB.update_data("vehicle", "Vehicle_ID",(String)deli.get("Vehicle_ID"), query);
					flag2 = insDB.PutDB.update_data("driver", "Driver_ID",(String)deli.get("Driver_ID"), query);
					flag2 = deleteDB.DelDB.remove_alldat("product", query2);
					if(flag&&flag2){
						RequestDispatcher view = req.getRequestDispatcher("rDeliver");
						view.forward(req, resp);
					}
				}else if(name_dat.equalsIgnoreCase("customer")){
					flag = deleteDB.DelDB.remove_alldat(name_dat, "Customer_ID", dat_id);
					if(flag){
						RequestDispatcher view = req.getRequestDispatcher("rCustomer");
						view.forward(req, resp);
					}
				}else if(name_dat.equalsIgnoreCase("device")){
					DBObject devi = getdat.getDatabase.getCollData("device", "Device_ID", dat_id);
					flag = deleteDB.DelDB.remove_alldat(name_dat, "Device_ID", dat_id);
					BasicDBObject query =new BasicDBObject();
					query.put("Device_ID", "");
					flag2 = insDB.PutDB.update_data("vehicle", "Vehicle_ID",(String)devi.get("Vehicle_ID"), query);
					if(flag){
						RequestDispatcher view = req.getRequestDispatcher("rDevice");
						view.forward(req, resp);
					}
				}else if(name_dat.equalsIgnoreCase("driver")){
					DBObject driv = getdat.getDatabase.getCollData("driver", "Driver_ID", dat_id);
					String drivch = (String) driv.get("stat");
					if(drivch.equalsIgnoreCase("0")){
						flag = deleteDB.DelDB.remove_alldat(name_dat, "Driver_ID", dat_id);
						if(flag){
							RequestDispatcher view = req.getRequestDispatcher("rDriver");
							view.forward(req, resp);
						}
					}else{
						resp.setContentType("text/html");
						PrintWriter out =resp.getWriter();
						// web code
						out.println(" <!DOCTYPE html>"); 
						out.println("<html>"); 
						out.println("<head>");
						out.println("<script type=\"text/javascript\"");
						out.println("src=\"./njs/editdb.js\">");
						out.println("</script>");
						out.println("</head>");
						out.println("<body >");
						out.println("can't delete Driver is on Deliver");
						out.println("</body>");					
						out.println("</html>");
						
					}
				}else if(name_dat.equalsIgnoreCase("vehicle")){					
					DBObject veh = getdat.getDatabase.getCollData("vehicle", "Vehicle_ID", dat_id);
					String vehch = (String) veh.get("stat");
					if(vehch.equalsIgnoreCase("0")){
						flag = deleteDB.DelDB.remove_alldat(name_dat, "Vehicle_ID", dat_id);
						BasicDBObject query =new BasicDBObject();
						BasicDBObject query2 =new BasicDBObject();
						query.put("stat", "0");
						query2.put("Vehicle_ID", "");
						flag2 = insDB.PutDB.update_data("device", "Device_ID",(String)veh.get("Device_ID"), query);
						flag2 = insDB.PutDB.update_data("device", "Device_ID",(String)veh.get("Device_ID"), query2);
						if(flag){
							RequestDispatcher view = req.getRequestDispatcher("rVehicle");
							view.forward(req, resp);
						}
					}else{
						resp.setContentType("text/html");
						PrintWriter out =resp.getWriter();
						// web code
						out.println(" <!DOCTYPE html>"); 
						out.println("<html>"); 
						out.println("<head>");
						out.println("<script type=\"text/javascript\"");
						out.println("src=\"./njs/editdb.js\">");
						out.println("</script>");
						out.println("</head>");
						out.println("<body >");
						out.println("can't delete Vehicle is on Deliver");
						out.println("</body>");					
						out.println("</html>");
						
					}
				}else if(name_dat.equalsIgnoreCase("product")){
					DBObject pro = getdat.getDatabase.getCollData("product", "Product_ID", dat_id);
					String d_id =(String) pro.get("Deliver_ID");
					flag = deleteDB.DelDB.remove_alldat(name_dat, "Product_ID", dat_id);
					if(flag){
						RequestDispatcher view = req.getRequestDispatcher("viewProduct.jsp?dv="+d_id+"");
						view.forward(req, resp);
					}
				}
				else if(name_dat.equalsIgnoreCase("eventdata")){
					String veh=req.getParameter("veh");
					BasicDBObject query1 = new BasicDBObject();
					query1.put(fild, dat_id);
					query1.put("Vehicle_ID", veh);
					flag = deleteDB.DelDB.remove_alldat(name_dat, query1);
					if(flag){
						RequestDispatcher view = req.getRequestDispatcher("report.php?Vehicle_ID="+req.getParameter("veh")+"");
						view.forward(req, resp);
					}
				}else if(name_dat.equalsIgnoreCase("del_mat")){
					//System.out.println(dat_id);
					String manu_id=new String(dat_id.getBytes("ISO-8859-1"), "UTF8");
					//System.out.println(manu_id);
					BasicDBObject query_m= new BasicDBObject();
					query_m.put("Company_ID", company_id);
					query_m.put("Manufacture_ID", manu_id);
					boolean f1 = deleteDB.DelDB.remove_alldat("manufacture", query_m);
					if(f1){
						boolean f2= deleteDB.DelDB.remove_alldat("material", query_m);
						if(f2){
							RequestDispatcher view = req.getRequestDispatcher("./raw_material/read_buy_raw.jsp");
							view.forward(req, resp);
						}else{
							resp.setContentType("text/html");
							PrintWriter out =resp.getWriter();
							// web code
							out.println(" <!DOCTYPE html>"); 
							out.println("<html>"); 
							out.println("<head>");
							out.println("<script type=\"text/javascript\"");
							out.println("src=\"./njs/editdb.js\">");
							out.println("</script>");
							out.println("</head>");
							out.println("<body >");
							out.println("Delete Manufacture Success but failed in Material");
							out.println("</body>");					
							out.println("</html>");
						}
					}else{
						resp.setContentType("text/html");
						PrintWriter out =resp.getWriter();
						// web code
						out.println(" <!DOCTYPE html>"); 
						out.println("<html>"); 
						out.println("<head>");
						out.println("<script type=\"text/javascript\"");
						out.println("src=\"./njs/editdb.js\">");
						out.println("</script>");
						out.println("</head>");
						out.println("<body >");
						out.println("Delete Manufacture failed!!!");
						out.println("</body>");					
						out.println("</html>");
					}					
				}else{
					String wr="Wrong ID or Password";
					req.setAttribute("wrong", wr);
					RequestDispatcher view = req.getRequestDispatcher("start.jsp?p1=123456");
					view.forward(req, resp);
				}									
				if(!flag){
					resp.setContentType("text/html");
					PrintWriter out =resp.getWriter();
					// web code
					out.println(" <!DOCTYPE html>"); 
					out.println("<html>"); 
					out.println("<head>");
					out.println("<script type=\"text/javascript\"");
					out.println("src=\"./njs/editdb.js\">");
					out.println("</script>");
					out.println("</head>");
					out.println("<body >");
					out.println("</body>");					
					out.println("</html>");
				}
			}
			//System.out.println(usn+"\n"+pwd+"\n"+user_stat);
			
			
			
		}
		System.gc();
		
	}
	

}