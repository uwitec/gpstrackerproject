package editRead;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.DBObject;

public class R_deliver extends HttpServlet {
	static String usn="";
	static String pwd ="";
	static String user_stat="";
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp, false);
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp, true);
	}
	public static void doWork(HttpServletRequest req, HttpServletResponse resp,boolean isPost)throws ServletException, IOException {
		
		HttpSession ses = req.getSession();
		ses.setAttribute("wr1", "Session out");
		usn =(String) ses.getAttribute("usn");
		pwd =(String) ses.getAttribute("pwd");
		user_stat=getdat.getDatabase.getUserdata(usn, pwd);
		String company_id = (String) ses.getAttribute("company_id");
		ses.setAttribute("company_id", company_id);
		ses.setAttribute("task", "deliver");
		if(user_stat.equals("")){
			
			String wr="Wrong ID or Password";
			req.setAttribute("wrong", wr);
			RequestDispatcher view = req.getRequestDispatcher("start.jsp?p1=123456");
			view.forward(req, resp);
						    
		}		
		RequestDispatcher view = req.getRequestDispatcher("readPage.jsp");
		view.forward(req, resp);
		//System.out.println(usn+"\n"+pwd+"\n"+user_stat);
		int row = 0;
		row = getdat.getDatabase.getRowCount("deliver", "Company_ID", company_id);
		DBObject[] deviceDat =null;
		deviceDat=getdat.getDatabase.getCollData("deliver", "Company_ID", company_id, row);
		String[] deliver_id =new String[row+1];
		String[] vehicle_id =new String[row+1];
		String[] driver_id=new String[row+1];
		String[] destination=new String[row+1];
		String[] origin=new String[row+1];
		String[] stat=new String[row+1];
		for(int i=1;i<=row;i++){
			driver_id[i]=(String) deviceDat[i].get("Driver_ID");
			deliver_id[i]=(String) deviceDat[i].get("Deliver_ID");
			vehicle_id[i]=(String) deviceDat[i].get("Vehicle_ID");
			stat[i]=(String) deviceDat[i].get("stat");
			origin[i]=(String) deviceDat[i].get("Origin");
			destination[i]=(String) deviceDat[i].get("Destination");
			
			
		}
		resp.setContentType("text/html");
		PrintWriter out =resp.getWriter();
		// web code
		out.println(" <!DOCTYPE html>"); 
		out.println("<html>"); 
		out.println("<head>");
		
		out.println("</head>");

		out.println("<body >");
		out.println("<table border=\"1\">");
		out.println("<caption>Deliver Detail</caption>");
		out.println("<tr><th>Deliver ID</th><th>Driver ID</th><th>Vehicle ID</th><th>stat</th><th>Origin</th><th>Destination</th></tr>");
		for(int i=1;i<=row;i++){
			out.println("<tr><td>"+deliver_id[i]+"</td><td>"+driver_id[i]+"</td><td>"+vehicle_id[i]+ "</td><td>"+stat[i]+"</td><td>"+origin[i]+"</td><td>"+destination[i]+"</td>");
			out.println("<td>");
			out.println("<form name=\"formmy"+i+"\"action=\"E_deliver\" method=\"post\">");
			//out.println("<input type=\"checkbox\" id=\"dv\" checked=\"checked\" name=\"dv\" value=\"testttttt\" /> ");
			out.println("<input type=\"button\" value=\"Product Detail\" onclick=\"window.location.href='./viewProduct.jsp?dv="+deliver_id[i]+"'\"/>");
			out.println("<input type=\"button\" value=\"Edit\" onclick=\"window.location.href='./E_edit?dv="+deliver_id[i]+"&dd=0&task=deliver'\"/>");
			out.println("<input type=\"button\" value=\"Delete\" onclick=\"window.location.href='./E_edit?dv="+deliver_id[i]+"&dd=yes&task=deliver'\"/>");
			out.println("</form>");
			out.println("</td>");
			out.println("</tr>");
		}
		out.println("</table>");
		out.println("</body>");
		
		out.println("</html>");
		
	}
	

}