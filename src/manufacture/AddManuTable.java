package manufacture;

import getdat.getDatabase;
import insDB.PutDB;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.BasicDBObject;

public class AddManuTable extends HttpServlet {	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp,false);
	}	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp,true);
	}
	public void doWork(HttpServletRequest req, HttpServletResponse resp,boolean isPost)
			throws ServletException, IOException {
		HttpSession ses = req.getSession();
		String usn = (String) ses.getAttribute("usn");
		String pwd = (String) ses.getAttribute("pwd");
		String user_stat = (String) ses.getAttribute("user_stat");
		if(user_stat.equals("")||user_stat==null){
			
			String wr="Wrong ID or Password";
			req.setAttribute("wrong", wr);
			RequestDispatcher view = req.getRequestDispatcher("start.jsp?note="+wr+"");
			view.forward(req, resp);
						    
			}
		
		resp.setContentType("text/html;charset=utf-8");
		PrintWriter out =resp.getWriter();
		
			//out.println("<%@ page language=\"java\" contentType=\"text/html; charset=utf-8\"pageEncoding=\"utf-8\"%>");
			out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
			out.println("<html>");
			out.println("<head>");
			out.println("<script type=\"text/javascript\">");
			out.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
			out.println("</script>");
			out.println("</head>");
			int num_pm= Integer.parseInt(req.getParameter("num_pm"));
			String manu_id = new String(req.getParameter("manu_id").getBytes("ISO-8859-1"), "UTF8");
			usn=(String) ses.getAttribute("usn");
			pwd=(String) ses.getAttribute("pwd");
			user_stat=(String) ses.getAttribute("user_stat");
			String company_id=(String) ses.getAttribute("company_id");
			BasicDBObject query1 =new BasicDBObject();
			query1.put("Company_ID", company_id);
			query1.put("Manufacture_ID",manu_id);			
			boolean f1 =deleteDB.DelDB.remove_alldat("manufacture_table", query1);
			if(f1){
				out.println("del finnish");				
				for(int i=1;i<=num_pm;i++){
					BasicDBObject query2 =new BasicDBObject();
					query2.put("Company_ID", company_id);
					query2.put("Manufacture_ID",manu_id);
					query2.put("Week", ""+i);
					query2.put("inhand", req.getParameter("inhand_week_"+i));
					query2.put("amount", req.getParameter("amount_week_"+i));
					query2.put("sell_forecast", req.getParameter("fore_week_"+i));
					query2.put("left", req.getParameter("left_week_"+i));
					boolean f2 =insDB.PutDB.insCollData("manufacture_table", query2);
					if(f2){
					out.println("week "+i+"  finish <br />");
					}else{
						out.println("week "+i+"  failed <br />");	
					}
				}
			}else{
				out.println("del failed");
			}
			out.println("asdasdasdasdasdasd");
			out.println("</body>");
			out.println("</html>");
		
	}

}