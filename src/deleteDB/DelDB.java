package deleteDB;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;

public class DelDB extends getdat.getDatabase {
	public static boolean remove_alldat(String collection,String fild,String dat){

		boolean flag=false;
		DBCollection coll;
		BasicDBObject query;
		Mongo m;
		DB db;
		
		try{
			m = getMongoConnection();
			db = m.getDB( "gts" );
			coll = db.getCollection(collection);
			query = new BasicDBObject();
			query.put(fild, dat);			
			coll.remove(query);
			flag=true;				
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return flag;
	}
	public static boolean remove_alldat(String collection,BasicDBObject query){

		boolean flag=false;
		DBCollection coll;		
		Mongo m;
		DB db;
		BasicDBObject query2=new BasicDBObject();
		try{
			m = getMongoConnection();
			db = m.getDB( "gts" );
			coll = db.getCollection(collection);
			query2.put("$in", query);
			coll.remove(query);
			flag=true;				
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return flag;
	}
}
