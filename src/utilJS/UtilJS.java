package utilJS;

import insDB.PutDB;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.*;


public class UtilJS extends HttpServlet {
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp,false);
	}

	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doWork(req, resp,true);
	}
	public void doWork(HttpServletRequest req, HttpServletResponse resp,boolean isPost)
			throws ServletException, IOException {
		HttpSession ses = req.getSession();
		String usn = (String) ses.getAttribute("usn");
		String pwd = (String) ses.getAttribute("pwd");
		String user_stat = (String) ses.getAttribute("user_stat");
		String company_name=(String) ses.getAttribute("company_name");		
		if(user_stat.equals("")||user_stat==null){
			System.out.println("get in util not found user stat");
			String wr="Wrong ID or Password";
			req.setAttribute("wrong", wr);
			RequestDispatcher view = req.getRequestDispatcher("start.jsp?note="+wr+"");
			view.forward(req, resp);
						    
			}
		String param=req.getParameter("param");
		resp.setContentType("text/xml;charset=utf-8");
		PrintWriter out =resp.getWriter();
		if(param.equalsIgnoreCase("vehdetail")){
			String vehicle_id=req.getParameter("vehid");
			String regid=(String) getdat.getDatabase.getCollData("vehicle", "Vehicle_ID", vehicle_id).get("RegisterID");
			String deliver_id = getdat.getDatabase.getActiveDeliverID(vehicle_id);
			String driver_id,driver_name;
			if(deliver_id.equalsIgnoreCase("notactive")){
				driver_id="notactive";
				driver_name="notactive";
			}else{
				driver_id = (String) getdat.getDatabase.getCollData("deliver", "Deliver_ID", deliver_id).get("Driver_ID");
				driver_name = (String) getdat.getDatabase.getCollData("driver", "Driver_ID", driver_id).get("First_name")+"  "+(String) getdat.getDatabase.getCollData("driver", "Driver_ID", driver_id).get("Last_name");
			}
			 out.println("<?xml version=\"1.0\"?>");
			 out.println("<CD>");
			out.print("<DELIVER_ID>"+deliver_id+"</DELIVER_ID>\n"+
					"<REGID>"+regid+"</REGID>\n"+
					"<DRIVER>"+driver_name+"</DRIVER>\n"+
					"<COMPANY>Columbia</COMPANY>\n"+
					"<PRICE>10.90</PRICE>\n"+
					"<YEAR>1985</YEAR>\n");
			out.println("</CD>");
			
			
		}else if(param.equalsIgnoreCase("upGmap")){
			String[] time=null;
			String vehicle_id=req.getParameter("vehid");
			int row_old=Integer.parseInt(req.getParameter("row_old"));
			//System.out.println("row old is :"+row_old);
			String deliver_id = getdat.getDatabase.getActiveDeliverID(vehicle_id);
			//int row=getdat.getDatabase.getRowCount("eventdata", "Deliver_ID", deliver_id);
			BasicDBObject rowdat=new BasicDBObject();
			rowdat.put("Deliver_ID", deliver_id);
			rowdat.put("Vehicle_ID", vehicle_id);
			int row=getdat.getDatabase.getRowCount("eventdata", rowdat);
			if(deliver_id.equalsIgnoreCase("notactive")){
				//System.out.println(vehicle_id+" not active in util");
			}else if(row==row_old){
				//System.out.println("EQUALS");
			}else if(row>row_old){
				double[] lat=null;
				double[] lon=null;
				if(row>0){
					lat= new double[row+1];
					lon= new double[row+1];
					time=getdat.getDatabase.geteventdata(lat, lon, deliver_id, vehicle_id);
					
				}
				 out.println("<?xml version=\"1.0\"?>");	
				 out.println("<EVENTDATA2>");
				 for(int i=1;i<=row;i++){
					 out.println("<EVENTDATA>");
					 out.print("<LAT>"+lat[i]+"</LAT>\n"+
							 	"<LON>"+lon[i]+"</LON>"+
							 	"<TIME>"+time[i]+"</TIME>");					 			
					 out.println("</EVENTDATA>");
				 }
				 row_old=row;
				 out.println("<ROW>");
				 out.println("<ROWOLD>"+row_old+"</ROWOLD>");
				 out.println("</ROW>");
				 out.println("</EVENTDATA2>");
				 row_old=row;
				 System.out.println("updating..........."+row_old+"...... "+row);
				
				 
			}else{
				//System.out.println(row+"  new "+row_old);
			}
			
		}else if(param.equalsIgnoreCase("Emarker")){
			DBObject eventdat=null;
			String timestamp=req.getParameter("time");			
			eventdat=getdat.getDatabase.getCollData("eventdata", "timestamp", timestamp);
			//System.out.println(eventdat.get("timestamp"));
			out.println("<?xml version=\"1.0\"?>");	
			 out.println("<EVENTDATA2>");			 
				 out.println("<EVENTDATA>");
				 out.print("<DELIVERID>Deliver ID :"+eventdat.get("Deliver_ID")+"</DELIVERID>\n"+
						 	"<COMMAND>Command :"+eventdat.get("command")+"</COMMAND>"+
						 	"<CODE>Event CODE :"+eventdat.get("code")+"</CODE>"+
						 	"<LAT>Latitude :"+eventdat.get("lat")+"</LAT>"+
						 	"<LON>Longtitude :"+eventdat.get("lon")+"</LON>"+
						 	"<TIME>Time :"+eventdat.get("Year")+"/"+eventdat.get("Month")+"/"+eventdat.get("Day")+":"+eventdat.get("Hour")+":"+eventdat.get("min")+":"+eventdat.get("sec")+"</TIME>"+
						 	"<INDI>GPS status indicator	:"+eventdat.get("GPS_status_indicator")+"</INDI>"+
						 	"<SA>Satellites Available :"+eventdat.get("satellites_available")+"</SA>"+
						 	"<SIGNAL>GSM Signal	:"+eventdat.get("GSM_signal")+"</SIGNAL>"+
						 	"<SPEED>Speed :"+eventdat.get("Speed")+" km/hr</SPEED>"+
						 	"<HEAD>Heading :"+eventdat.get("heading")+"</HEAD>"+
						 	"<ALTITUDE>Altitude :"+eventdat.get("altitude")+"</ALTITUDE>"+						 	
						 	"<MILEAGE>Mileage :"+eventdat.get("Mileage")+"</MILEAGE>");					 			
				 out.println("</EVENTDATA>");
			 
			
			/* out.println("<ROW>");
			 out.println("<ROWOLD>"+row_old+"</ROWOLD>");
			 out.println("</ROW>");*/
			 out.println("</EVENTDATA2>");
			
		}else if(param.equalsIgnoreCase("up_raw")){
			String company_id=req.getParameter("company_id");
			String manu_id=new String(req.getParameter("manu_id").getBytes("ISO-8859-1"), "UTF8");
			//System.out.println(manu_id);
			DBObject[] matdat=null;
			BasicDBObject query = new BasicDBObject();
			query.put("Company_ID", company_id);
			query.put("Manufacture_ID", manu_id);
			int row=getdat.getDatabase.getRowCount("material", query);			
			matdat=getdat.getDatabase.getCollData("material", query, row);
			
			out.println("<?xml version=\"1.0\" encoding='utf-8'?>");	
			 out.println("<MATDATA>");
			 for(int i=1;i<=row;i++){
				 out.println("<MATDATA1>");
				 out.print("<MATID>"+matdat[i].get("Part_ID")+"</MATID>\n"+
						 	"<MATNAME>"+matdat[i].get("Part_Name")+"</MATNAME>"+
						 	"<MATDES>"+matdat[i].get("Part_description")+"</MATDES>"+
						 	"<MATA>"+matdat[i].get("Part_Amount")+"</MATA>"+
						 	"<MATW>"+matdat[i].get("Part_Weight")+"</MATW>"+
						 	"<MATV>"+matdat[i].get("Part_volume")+"</MATV>"+
						 	"<MATP>"+matdat[i].get("Part_Price")+"</MATP>"+
						 	"<MATS>"+matdat[i].get("Part_Seller")+"</MATS>"+
						 	"<MATSTAT>"+matdat[i].get("Part_Stat")+"</MATSTAT>");			 			
				 out.println("</MATDATA1>");
			 }
			 out.println("<ROW>");
			 out.println("<ROWOLD>"+row+"</ROWOLD>");
			 out.println("</ROW>");
				 out.println("</MATDATA>");
		}else if(param.equalsIgnoreCase("edit_send_stat_raw")){
			//System.out.println("in E raw!!");
			String usnm = req.getParameter("usn");
			String pwdm = req.getParameter("pwd");
			String role=getdat.getDatabase.getUserdata(usnm, pwdm);
			String company_id=(String)ses.getAttribute("company_id");
			if(role.equalsIgnoreCase("company")){
				//System.out.println("pass user");
				BasicDBObject query3 = new BasicDBObject();
				query3.put("Company_ID", company_id);
				query3.put("username", usnm);
				DBObject[] usercom=getdat.getDatabase.getCollData("company", query3, 1);
				DBObject poshu=getdat.getDatabase.getCollData("userdata", "username", usnm);
				String positionhuman=(String) poshu.get("position");
				if(positionhuman==null||usercom[1]==null){
					positionhuman="not in position";
				}else{			
			String manu_id=new String(req.getParameter("manu_id").getBytes("ISO-8859-1"), "UTF8");
			String mat_id=new String(req.getParameter("mat_id").getBytes("ISO-8859-1"), "UTF8");
			//System.out.println(manu_id);
			String oldstat=req.getParameter("old_stat");
			DBObject[] matdat=null;
			BasicDBObject query = new BasicDBObject();
			BasicDBObject dat_up = new BasicDBObject();
			query.put("Company_ID", company_id);
			query.put("Manufacture_ID", manu_id);
			query.put("Part_ID", mat_id);
			if(oldstat.equalsIgnoreCase("1")){
				dat_up.put("Part_Stat", "0");
			}else{
				dat_up.put("Part_Stat", "1");
				dat_up.put("Part_Stat_approve_by", usnm);
			}
			boolean f1 = insDB.PutDB.update_data("material", query, dat_up);
			if(f1){
				matdat=getdat.getDatabase.getCollData("material", query, 1);
				String raw_send_stat=(String) matdat[1].get("Part_Stat");
				out.println("<?xml version=\"1.0\" encoding='utf-8'?>");	
				out.println("<MATSTAT>");
				out.println("<MATSTAT1>"+raw_send_stat+"</MATSTAT1>");
			
				 out.println("</MATSTAT>");
			}else{
				System.out.println("up failed!!");
			}
				}
			}
			
		}else if(param.equalsIgnoreCase("up_manufacture_build_stat")){
			//System.out.println("in E raw!!");
			String company_id=(String)ses.getAttribute("company_id");
			String manu_id=new String(req.getParameter("manu_id").getBytes("ISO-8859-1"), "UTF8");
			
			//System.out.println(manu_id);
			
			DBObject[] matdat=null;
			BasicDBObject query = new BasicDBObject();
			BasicDBObject dat_up = new BasicDBObject();
			query.put("Company_ID", company_id);
			query.put("Manufacture_ID", manu_id);
			int row=getdat.getDatabase.getRowCount("material", query);			
			matdat=getdat.getDatabase.getCollData("material", query, row);
			boolean f1 =false;
			String ch_b;
			for(int i=1;i<=row&&!f1;i++){
				ch_b=(String)matdat[i].get("Part_Stat");
				if(ch_b.equalsIgnoreCase("0")){
					f1=true;
				}
			}
			if(f1){
				dat_up.put("Part_Stat", "0");
			}else{
				dat_up.put("Part_Stat", "1");
			}
			boolean f2 = insDB.PutDB.update_data("manufacture", query, dat_up);
			if(f2){
				matdat=getdat.getDatabase.getCollData("manufacture", query, 1);
				String raw_send_stat=(String) matdat[1].get("Part_Stat");
				out.println("<?xml version=\"1.0\" encoding='utf-8'?>");	
				out.println("<MATSTAT>");
				out.println("<MATSTAT1>"+raw_send_stat+"</MATSTAT1>");
			
				 out.println("</MATSTAT>");
			}else{
				System.out.println("up failed!!222");
			}
		}else if(param.equalsIgnoreCase("up_manufacture_build2_stat")){
			//System.out.println("in E raw2!!");
			String company_id=(String)ses.getAttribute("company_id");
			String manu_id=new String(req.getParameter("manu_id").getBytes("ISO-8859-1"), "UTF8");
			
			//System.out.println(manu_id);
			
			DBObject[] manudat=null;
			BasicDBObject query = new BasicDBObject();
			BasicDBObject dat_up = new BasicDBObject();
			query.put("Company_ID", company_id);
			query.put("Manufacture_ID", manu_id);					
			manudat=getdat.getDatabase.getCollData("manufacture", query, 1);
			boolean f1 =false;
			String ch_b;
			
				ch_b=(String)manudat[1].get("Build_Stat");
				if(ch_b.equalsIgnoreCase("0")){
					f1=true;
				}
			
			if(f1){
				dat_up.put("Build_Stat", "1");
			}else{
				dat_up.put("Build_Stat", "0");
			}
			boolean f2 = insDB.PutDB.update_data("manufacture", query, dat_up);
			if(f2){
				manudat=getdat.getDatabase.getCollData("manufacture", query, 1);
				String raw_send_stat=(String) manudat[1].get("Build_Stat");
				out.println("<?xml version=\"1.0\" encoding='utf-8'?>");	
				out.println("<MATSTAT>");
				out.println("<MATSTAT1>"+raw_send_stat+"</MATSTAT1>");
			
				 out.println("</MATSTAT>");
			}else{
				System.out.println("up failed!!222");
			}
		}else if(param.equalsIgnoreCase("approve_manufacture")){
			//System.out.println("in E raw2!!");
			String usnm = req.getParameter("usn");
			String pwdm = req.getParameter("pwd");
			String role=getdat.getDatabase.getUserdata(usnm, pwdm);
			String company_id=(String)ses.getAttribute("company_id");
			if(role.equalsIgnoreCase("company")){
				//System.out.println("pass user");
				BasicDBObject query3 = new BasicDBObject();
				query3.put("Company_ID", company_id);
				query3.put("username", usnm);
				DBObject[] usercom=getdat.getDatabase.getCollData("company", query3, 1);
				DBObject poshu=getdat.getDatabase.getCollData("userdata", "username", usnm);
				String positionhuman=(String) poshu.get("position");
				if(positionhuman==null||usercom[1]==null){
					positionhuman="not in position";
				}else{				
				String manu_id=new String(req.getParameter("manu_id").getBytes("ISO-8859-1"), "UTF8");
			
				DBObject[] manudat=null;
				BasicDBObject query = new BasicDBObject();
				BasicDBObject dat_up = new BasicDBObject();
				query.put("Company_ID", company_id);
				query.put("Manufacture_ID", manu_id);					
				manudat=getdat.getDatabase.getCollData("manufacture", query, 1);				
				String ch_b;			
					ch_b=(String)manudat[1].get("Build_Approve");
					if(ch_b==null){
						ch_b="0";
					}
					if(ch_b.equalsIgnoreCase("0")&&positionhuman.equalsIgnoreCase("manager")){
					
						dat_up.put("Build_Approve", "1");
						dat_up.put("Build_Approve_by", usnm);
					}			
					
					boolean f2 = insDB.PutDB.update_data("manufacture", query, dat_up);
					if(f2){
						manudat=getdat.getDatabase.getCollData("manufacture", query, 1);
						String raw_send_stat=(String) manudat[1].get("Build_Approve");
						out.println("<?xml version=\"1.0\" encoding='utf-8'?>");	
						out.println("<MATSTAT>");
					out.println("<MATSTAT1>"+raw_send_stat+"</MATSTAT1>");
			
					out.println("</MATSTAT>");
					}else{
						System.out.println("up failed!!111");
					}
				}
			}
		}
		else if(param.equalsIgnoreCase("approve_buy_manufacture")){
			//System.out.println("in E raw2!!");
			String usnm = req.getParameter("usn");
			String pwdm = req.getParameter("pwd");
			String role=getdat.getDatabase.getUserdata(usnm, pwdm);
			String company_id=(String)ses.getAttribute("company_id");
			if(role.equalsIgnoreCase("company")){
				//System.out.println("pass user");
				BasicDBObject query3 = new BasicDBObject();
				query3.put("Company_ID", company_id);
				query3.put("username", usnm);
				DBObject[] usercom=getdat.getDatabase.getCollData("company", query3, 1);
				DBObject poshu=getdat.getDatabase.getCollData("userdata", "username", usnm);
				String positionhuman=(String) poshu.get("position");
				if(positionhuman==null||usercom[1]==null){
					positionhuman="not in position";
				}else{				
				String manu_id=new String(req.getParameter("manu_id").getBytes("ISO-8859-1"), "UTF8");
			
				DBObject[] manudat=null;
				BasicDBObject query = new BasicDBObject();
				BasicDBObject dat_up = new BasicDBObject();
				query.put("Company_ID", company_id);
				query.put("Manufacture_ID", manu_id);					
				manudat=getdat.getDatabase.getCollData("manufacture", query, 1);				
				String ch_b;			
					ch_b=(String)manudat[1].get("Buy_Approve");
					if(ch_b==null){
						ch_b="0";
					}
					if(ch_b.equalsIgnoreCase("0")&&positionhuman.equalsIgnoreCase("manager")){
					
						dat_up.put("Buy_Approve", "1");
						dat_up.put("Buy_Approve_by", usnm);
					}			
					
					boolean f2 = insDB.PutDB.update_data("manufacture", query, dat_up);
					if(f2){
						manudat=getdat.getDatabase.getCollData("manufacture", query, 1);
						String raw_send_stat=(String) manudat[1].get("Buy_Approve");
						out.println("<?xml version=\"1.0\" encoding='utf-8'?>");	
						out.println("<MATSTAT>");
					out.println("<MATSTAT1>"+raw_send_stat+"</MATSTAT1>");
			
					out.println("</MATSTAT>");
					}else{
						System.out.println("up failed!!111");
					}
				}
			}
		}else if(param.equalsIgnoreCase("vendor_pointer_change")){
			String v_id = new String(req.getParameter("v_id").getBytes("ISO-8859-1"), "UTF8");
			String company_id=(String) ses.getAttribute("company_id");
			DBObject[] vendor=null;
			BasicDBObject query = new BasicDBObject();
			query.put("Company_ID", company_id);
			query.put("Vendor_Name", v_id);
			vendor=getdat.getDatabase.getCollData("vendor", query, 1);
			out.println("<?xml version=\"1.0\" encoding='utf-8'?>");	
			out.println("<VENDOR>");
			if(vendor==null){
				out.print("<VNAME>nothing</VNAME>");				
			}else{
				out.print("<VNAME>"+vendor[1].get("Vendor_Name")+"</VNAME>\n"+						
					 	"<VCONNAME>"+vendor[1].get("Vendor_Con_Name")+"</VCONNAME>"+
					 	"<VPHONE>"+vendor[1].get("Vendor_Phone")+"</VPHONE>"+
					 	"<VFAX>"+vendor[1].get("Vendor_Fax")+"</VFAX>"+
					 	"<VEMAIL>"+vendor[1].get("Vendor_Email")+"</VEMAIL>"+
					 	"<VWEBSITE>"+vendor[1].get("Vendor_Website")+"</VWEBSITE>"+
					 	"<VADDRESS>"+vendor[1].get("Vendor_Address")+"</VADDRESS>"+
					 	"<VPAYMETHOD>"+vendor[1].get("Vendor_Pay")+"</VPAYMETHOD>"+
					 	"<VTRAN>"+vendor[1].get("Vendor_Tran")+"</VTRAN>"+
					 	"<VCURRENCY>"+vendor[1].get("Vendor_Currency")+"</VCURRENCY>"+
					 	"<VREMARK>"+vendor[1].get("Vendor_Remark")+"</VREMARK>"+
					 	"<VTAX>"+vendor[1].get("Vendor_Tax")+"</VTAX>");
			}
			out.println("</VENDOR>");
			
		}else if(param.equalsIgnoreCase("vendor_save")){
			String v_name = req.getParameter("v_name");
			String v_con_name = req.getParameter("v_con_name");
			String v_phone = req.getParameter("v_phone");
			String v_fax = req.getParameter("v_fax");
			String v_email = req.getParameter("v_email");
			String v_website = req.getParameter("v_website");
			String v_address = req.getParameter("v_address");
			String v_pay_method = req.getParameter("v_pay_method");
			String v_tran = req.getParameter("v_tran");
			String v_currency = req.getParameter("v_currency");
			String v_tax = req.getParameter("v_tax");
			String v_remark = req.getParameter("v_remark");
			String company_id=(String) ses.getAttribute("company_id");
			/*System.out.println(" "+v_name+" "+v_con_name+" "+v_phone+
					" "+ v_fax+" "+v_email +" "+v_website +" "+ v_address+" "+
					v_pay_method+" "+ v_tran+" "+v_currency +" "+ v_tax+" "+v_remark);*/
			boolean f1=PutDB.ins_vendor(company_id, v_name, v_con_name, v_phone, v_fax, v_email, v_website, v_address, v_pay_method, v_tran, v_currency, v_tax, v_remark);
			out.println("<?xml version=\"1.0\" encoding='utf-8'?>");	
			out.println("<VENDOR>");
			if(f1){				
				out.print("<VADDSTAT>yes</VADDSTAT>\n");
				
			}
			else{
				out.print("<VADDSTAT>no</VADDSTAT>\n");
			}
			out.println("</VENDOR>");
		}else if(param.equalsIgnoreCase("find_dat")){
			String company_id=(String) ses.getAttribute("company_id");
			String collName = req.getParameter("collName");
			DBObject[] datacoll=null;
			BasicDBObject query = new BasicDBObject();
			query.put("Company_ID", company_id);
			int row = getdat.getDatabase.getRowCount(collName, query);
			BasicDBObject query1 = new BasicDBObject();
			query1.put("Company_ID", company_id);
			BasicDBObject query2 = new BasicDBObject();
			datacoll=getdat.getDatabase.getCollData(collName, query1, row);
			out.println("<?xml version=\"1.0\" encoding='utf-8'?>");	
			out.println("<VENDOR>");
			for(int i=1;i<=row;i++){
				out.println("<VNAME>"+datacoll[i].get("Vendor_Name")+"</VNAME>");
			}
			out.println("<ROW>"+row+"</ROW>");
			out.println("</VENDOR>");
			
			
			
		}
		
		//System.out.println("get in util");
		System.gc();
		
		
	}

}
